<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start(); //we need to call PHP's session object to access it through CI
setlocale(LC_ALL, "pt_BR", "pt_BR.iso-8859-1", "pt_BR.utf-8", "portuguese");
  date_default_timezone_set('America/Sao_Paulo');
class Novidades extends CI_Controller {
  
  public $data;

 function __construct()
 {

   parent::__construct();
   $this->data['page'] = 'novidades';
   $this->data['contato'] = $this->db->get('contato_principal')->row_array();
   $this->data['parceiros'] = $this->db->get('parceiros')->result_array();
   $this->data['novidades'] = $this->db->order_by('data_publicacao', 'desc')->get('novidades', 4)->result_array();
 }

 function index($slug = false)
 {
  
  if($slug) {
    $this->data['novidade_destaque'] = $this->db->get_where('novidades', array('slug' => $slug))->row_array();
    foreach($this->data['novidades'] as $k => $v){
        if($v['slug'] == $this->data['novidade_destaque']['slug']){
          $this->data['index_anterior'] = $k;
          $this->data['index_proxima'] = $k + 2;
        }
     }
     $this->data['title'] = 'Novidades';
     $this->load->view('layout/template', $this->data);
  }
  else{
    $this->data['page'] = 'todas';
    $this->data['title'] = 'Novidades';
    $this->load->view('layout/template', $this->data);

  }


    
 }
 

}



?>

