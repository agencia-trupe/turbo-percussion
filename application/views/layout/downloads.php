<div class="banner banner-produtos">
	<div class='wrapper-banner'>Downloads</div>
</div>

<div class="container-corpo">
	<div class="wrapper-corpo">
		<div class="titulo-endorses">Faça o download de manuais, wallpapers e outros arquivos em PDF.</div>

		<div class="coluna-endorses-1">
			<?php foreach ($manuais_1 as $key => $v): ?>
				<div class="linha-endorses"><a href="<?php echo base_url('downloads/download/'.$v->id.'/manuais') ?>"><?php echo $v->nome ?></a>
				<div class="borda-novidade"></div></div>
			<?php endforeach ?></div>
		<div class="coluna-endorses-2">
			<?php foreach ($manuais_2 as $key => $v): ?>
				
				<div class="linha-endorses"><a href="<?php echo base_url('downloads/download/'.$v->id.'/manuais') ?>"><?php echo $v->nome ?></a>
				<div class="borda-novidade"></div></div>
			<?php endforeach ?></div>
		</div>
	</div>
	
</div>

<script>
	jQuery(document).ready(function($) {
		$('.cycle-box').cycle();
	});


</script>
