

<div class="row-fluid">
	<div class="col-lg-12">
		<?php if ($this->session->flashdata('message')): ?>
			<div class="alert alert-success"> <?php echo $this->session->flashdata('message') ?> </div>		
		<?php endif ?>
	</div>
</div>

<div class="row-fluid">
	<legend>Inserir <?php echo $title ?></legend>
</div>

<div class="clearfix"></div>

<form  enctype='multipart/form-data' action="<?php echo base_url('admin/'.$attr['slug'].'/'.$this->uri->segment(3)) ?>" method='post'>
	
	<input type="hidden" name='<?php echo $attr['slug'] ?>[id_subcategoria] ' value='<?php echo $this->session->userdata('subcategoria_selecionada') ?>'>
	<input type="hidden" id='contador_caracteristicas' value='0'>

	<div class="row-fluid">
		<div class="col-1 text-right">
			<label for="nome" class='control-label'>Nome</label>
		</div>
		<div class="col-3">
			<input class='form-control' type="text" name='<?php echo $attr['slug'] ?>[nome]' value='<?php echo ($obj) ? $obj->nome : '' ?>' id='nome'>	
		</div>
	</div>

	<div class="clearfix"></div>

	<div class="row-fluid">
		<div class="col-1 text-right">
			<label for="descricao" class='control-label'>Descrição</label>
		</div>
		<div class="col-3">
			<input class='form-control' type="text" name='<?php echo $attr['slug'] ?>[descricao]' value='<?php echo ($obj) ? $obj->descricao : '' ?>' id='descricao'>	
		</div>
	</div>

	<div class="clearfix"></div>

	<div class="row-fluid">
		<div class="col-1 text-right">
			<label for="informacoes" class='control-label'>Informações</label>
		</div>
		<div class="col-6">
			<textarea class='form-control mce' type="text" name='<?php echo $attr['slug'] ?>[informacoes]' value="" id='informacoes'><?php echo ($obj) ? $obj->informacoes : '' ?></textarea>	
		</div>
	</div>

	<div class="clearfix"></div>
	<div class="clearfix"></div>

	<div class="row-fluid">
		<div class="col-1 text-right">
			<label for="link" class='control-label'>Foto Principal</label>
		</div>
		<div class="col-3">
			<a id='UploadButton'><button class='btn btn-sm btn-info'>Selecionar Foto</button></a><br><br>	<div class="container_foto_unica">
					<img src="" id='foto_unica'  alt=""><input type="hidden" id='hidden_foto'  name='<?php echo $attr['slug'] ?>[imagem_principal]'  value=''> </div>
			
		</div>
	</div>

	<div class="clearfix"></div>

	<br><br>
	
	<div class="row-fluid">
		<div class="col-12">
	
			<legend>
				Tabela de Características:				
			</legend>

			<div class="col-12">
				<label for="0-colunas" class="radio-inline">
					<input name='n_colunas' class='n_colunas' value='0' type="radio" id='0-colunas' checked>
					Produto sem tabela de Características
				</label><br>
				<label for="3-colunas" class="radio-inline">
					<input name='n_colunas' class='n_colunas' value='3' type="radio" id='3-colunas'>
					3 Colunas
				</label><br>
				<label for="2-colunas" class="radio-inline">
					<input name='n_colunas' type="radio" class='n_colunas' value='2' id='2-colunas'>
					2 Colunas
				</label><br>				
			</div>

			<div class="formulario-caracteristicas">

				<div class="col-12" id="tabela-2col" style="margin:20px 0;">
					
					<table class="table table-striped" style="width:50%;margin:0 auto;">
						<thead>
							<tr>
								<td></td>
								<td><input type="text" name="2col[titulo-coluna1]" class="form-control" placeholder="Coluna 1"></td>
								<td></td>							
							</tr>
						</thead>
						<tbody>
							<tr>
								<td><input type="text" name="2col[linha][]" class="form-control" placeholder="Valor 1"></td>
								<td><input type="text" name="2col[linha][]" class="form-control" placeholder="Valor 2"></td>		
								<td><a href="" title="Remover Linha" class="remover-linha"><span class="glyphicon glyphicon-remove"></span></a></td>
							</tr>
						</tbody>
						<tfoot>
							<tr>
								<td colspan="3">
									<button type='button' class='btn btn-sm btn-success add_caracteristica'>
										<span class="glyphicon glyphicon-plus"> Adicionar Linha</span>
									</button>									
								</td>
							</tr>
						</tfoot>
					</table>


				</div>

				<div class="col-12" id="tabela-3col" style="margin:20px 0;">
					
					<table class="table table-striped" style="width:50%;margin:0 auto;">
						<thead>
							<tr>
								<td></td>
								<td><input type="text" name="3col[titulo-coluna1]" class="form-control" placeholder="Coluna 1"></td>
								<td><input type="text" name="3col[titulo-coluna2]" class="form-control" placeholder="Coluna 2"></td>
								<td></td>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td><input type="text" name="3col[linha][]" class="form-control" placeholder="Valor 1"></td>
								<td><input type="text" name="3col[linha][]" class="form-control" placeholder="Valor 3"></td>
								<td><input type="text" name="3col[linha][]" class="form-control" placeholder="Valor 3"></td>
								<td><a href="" title="Remover Linha" class="remover-linha"><span class="glyphicon glyphicon-remove"></span></a></td>
							</tr>
						</tbody>
						<tfoot>
							<tr>
								<td colspan="4">
									<button type='button' class='btn btn-sm btn-success add_caracteristica'>
										<span class="glyphicon glyphicon-plus"> Adicionar Linha</span>
									</button>									
								</td>
							</tr>
						</tfoot>
					</table>


				</div>
				
				<hr>

				<div class="clearfix"></div>

				<div class="row-fluid" style="margin-top:80px;">
					<div class="col-12">
						<legend>Fotos do Produto: <a class='UploadFoto' id='UploadFoto'><button class='btn btn-info btn-sm'>Adicionar Foto</button></a><span  class='text-info alterar_ordem_info'>Caso queira alterar a ordem das fotos, edite o produto após salvo.</span></legend>
					</div>
				</div>
				
				<div class="clearfix"></div>
				
				<div class="row-fluid">
					<input type="hidden" id='contador' value='0'>
						<div class="col-12">
							<div id="container_fotos_produto">
					
							</div>
						</div>
				</div>

				<hr>
				
				<div class="clearfix"></div>

				<div class="row-fluid">
					<div class="col-1 text-right">
						<label for="url_manual" class='control-label'>Manual</label>
					</div>
					<div class="col-4">
						<input class='form-control' type="file" name='userfile' id='url_manual'>	
					</div>
				</div>

				<div class="clearfix"></div>

				<div class="row-fluid">

				<div class="col-2">
					<button type='submit' class='btn btn btn-success'>Salvar</button>
					<button type='button' onClick='history.back()' class='btn btn btn-info'>Voltar</button>
				</div>
				
			</div>
		</div>
	</div>
	<div class="clearfix"></div>

</form>
<script>
jQuery(document).ready(function($) {
	
	$("input[name='n_colunas']").change( function(){
		if($("input[name='n_colunas']:checked").val() == 2){
		
			if($('#tabela-3col').is(':visible')){
				$('#tabela-3col').hide();
			}
			$('#tabela-2col').show();

		}else if($("input[name='n_colunas']:checked").val() == 3){
			
			if($('#tabela-2col').is(':visible')){
				$('#tabela-2col').hide();
			}
			$('#tabela-3col').show();			
		}else if($("input[name='n_colunas']:checked").val() == 0){
			$('#tabela-2col').css('display', 'none');
			$('#tabela-3col').css('display', 'none');			
		}
	});

	$('.add_caracteristica').click( function(e){
		e.preventDefault();

		var tabela = $(this).parent().parent().parent().parent();

		if($('#tabela-3col').is(':visible')){
			var linha = tabela.find('tbody tr:first').clone();
			linha.find('input').val('');
			$('#tabela-3col').find('tbody').append(linha);
		}else if($('#tabela-2col').is(':visible')){
			var linha = tabela.find('tbody tr:first').clone();
			linha.find('input').val('');
			$('#tabela-2col').find('tbody').append(linha);
		}
	});

	$('table.table').on("click", ".remover-linha", function(e){
		e.preventDefault();
		$(this).parent().parent().hide().remove();
	}); 
	
	$("#UploadButton").ajaxUpload({
		url : "<?php echo base_url('admin/'.$attr['slug'].'/upload_foto') ?>",
		name: "file",
		onSubmit: function() {
    		$('#InfoBox').html('Uploading ... ');
		},
		onComplete: function(result) {
			parsed = $.parseJSON(result);
		    $('#foto_unica').attr('src', parsed['url']).show();
		    $('#hidden_foto').val(parsed['filename']);
		}
	});

	$("#UploadFoto").ajaxUpload({
		url : "<?php echo base_url('admin/produtos/upload_foto') ?>",
		name: "file",
		onSubmit: function() {

		},
		onComplete: function(result) {
			contador = $('#contador').val()
			parsed = $.parseJSON(result)
			if(parsed['status'] == 'error'){
				alert(parsed['msg']);
			}else{
				$('#container_fotos_produto').append("<div class='foto_produto'><img src='"+parsed['url']+"'><input type='hidden' name='produto_fotos[]' value='"+parsed['filename']+"'> </div>");
				contador++;
				$('#contador').val(contador);
			}
		}
	});
});	

</script>
