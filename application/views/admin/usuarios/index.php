<div class="row-fluid">

	<div class="col-lg-12"><?php if ($this->session->flashdata('message')): ?>
		<div class="alert alert-success"> <?php echo $this->session->flashdata('message') ?> </div>
		
	<?php endif ?>
	</div>
</div>

<div class="row-fluid">
	<legend>Usuários 
		<a href="<?php echo base_url() ?>admin/usuarios/inserir ">
			<button class='btn btn-xs btn-success'>
				<span class='glyphicon glyphicon-plus'></span>
			</button>
		</a>
		
		
	</legend>
</div>
<div class="clearfix"></div>
<div class="row-fluid">
	<div class="col-2"></div>
	<div class="col-8">
		<table class='table table-stripped table-condensed'>
			<thead>
				
				<tr>
					<th>Nome</th><th>Ações</th>
				</tr>
			</thead>
			<tbody>
				
				<?php foreach ($usuarios as $i => $usuario): ?>
					<tr id='item_<?php echo $usuario->id ?>	'>
						<td><?php echo $usuario->username ?></td><td><a class='botao_tabela' href='<?php echo base_url() ?>admin/usuarios/editar/<?php echo $usuario->id ?>'><button title='Editar usuário' class='btn btn-xs btn-info'><span class="glyphicon glyphicon-pencil"></span></button></a>
						<a  onClick="if(confirm('Tem certeza que deseja deletar este usuário? Esta operação é irreversível')) window.location = '<?php echo base_url() ?>admin/usuarios/deletar/<?php echo $usuario->id; ?>'"><button title='Deletar usuário' class='btn btn-xs btn-info'><span class="glyphicon glyphicon-remove"></span></button></a> <!--<button class='btn btn-xs btn-info'><span class="glyphicon glyphicon-folder-open"></span></button>--></td>
					</tr>
				<?php endforeach ?>
			</tbody>
		</table>	
	</div>
</div>



 <script>
$("table tbody").sortable({


  		update: function( event, ui ) {
			data = $('table tbody').sortable('serialize')
			
			$.post('<?php echo base_url() ?>admin/usuarios/grava_ordems', data)
			
		}
});
  
  
</script>