<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start(); //we need to call PHP's session object to access it through CI
class Usuarios extends CI_Controller {

 function __construct()
 {
  
   parent::__construct();
 }

 function index()
 {

   if($this->session->userdata('logged_in'))
   {
     $session_data = $this->session->userdata('logged_in');
     $data['title'] = 'Usuários';
     $data['usuarios'] = $this->db->get('users')->result();
     $data['page'] = 'usuarios';
     $this->load->view('admin/template', $data);
   }
   else
   {
     //If no session, redirect to login page
     redirect('admin/login', 'refresh');
   }
 }
 
  function inserir()	
 {
     if(isSet($_POST) && $_POST){
    $post = $this->input->post('user');
 	  $user = array(
 	  	'username' => $post['username'],
 	  	'password' => md5($post['password'])
 	  );
      $this->db->insert('users', $user);
      $this->session->set_flashdata('message', "Usuário adicionado com sucesso!");
      redirect('admin/usuarios', 'refresh');
    }

   if($this->session->userdata('logged_in'))
   {
     $session_data = $this->session->userdata('logged_in');
 	    $data['title'] = 'Inserir Usuário';

     
     $data['page'] = 'inserir';
     $this->load->view('admin/template', $data);
   }
   else
   {
     //If no session, redirect to login page
     redirect('admin/login', 'refresh');
   }
 }
 
function deletar($id)	
 {
     
   if($this->session->userdata('logged_in'))
   {
     $session_data = $this->session->userdata('logged_in');
     $this->db->delete('users', array('id' => $id));
     $this->session->set_flashdata('message', "Usuário deletado com sucesso!");
     redirect('admin/usuarios', 'refresh');
   }
   else
   {
     //If no session, redirect to login page
     redirect('admin/login', 'refresh');
   }
 }
  
function editar($id)	
 {
     if(isSet($_POST) && $_POST){
 	  $post = $this->input->post('user');
    $user = array(
      'username' => $post['username'],
      'password' => md5($post['password'])
    );
      $this->db->update('users', $user, array('id' => $id));
      $this->session->set_flashdata('message', "Usuário editado com sucesso!");
      redirect('admin/usuarios', 'refresh');
    }

   if($this->session->userdata('logged_in'))
   {
     $session_data = $this->session->userdata('logged_in');
     $data['title'] = 'Editar Usuário';
     $data['usuario'] = $this->db->get_where('users', array('id' => $id))->row();
     $data['page'] = 'editar';
     $this->load->view('admin/template', $data);
   }
   else
   {
     //If no session, redirect to login page
     redirect('admin/login', 'refresh');
   }
 }
 
 
 

 function logout()
 {
   $this->session->unset_userdata('logged_in');
   session_destroy();
   redirect('admin/home', 'refresh');
 }

}

?>

