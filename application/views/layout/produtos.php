<div class="banner banner-produtos">
	<div class='wrapper-banner'>Produtos</div>
</div>

<div class="container-corpo">
	<div class="wrapper-corpo">
		<div class="coluna-categorias">
			<div class="titulo-categorias">Categorias</div>
			<?php foreach ($produto_categorias as $key => $produto_categoria): ?>
				<div class="linha-categoria"><span><?php echo $produto_categoria['titulo'] ?></span>
				
				</div>
				<div class="subcategorias">
					
					<?php foreach ($produto_categoria['produto_subcategorias'] as $k => $v): ?>
						<a href="<?php echo base_url('produtos/subcategoria/'.$v['slug']) ?>"><div class="linha-subcategoria <?php if ($slug && $slug == $v['slug']): ?> subcategoria-ativa<?php endif ?>"><?php echo $v['titulo'] ?></div></a>
						
					<?php endforeach ?>
				</div>
			<?php endforeach ?>	
		</div>
			<div class="borda-novidade-lateral-produto"></div>				
		<div class="coluna-produtos">
			<?php foreach ($produtos_destaque as $key => $produto): ?>
				<div class="container-produto-listagem">
					<a href="<?php echo base_url('produtos/visualizar/'.$produto['slug']) ?> "><img class='thumb-produto-listagem' src='<?php echo base_url('assets/img/uploads/produto_fotos/'.$produto['imagem_principal'].'_lista.jpg') ?>' alt='' ></a>
					<a href="<?php echo base_url('produtos/visualizar/'.$produto['slug']) ?> "><div class="titulo-produto-listagem"><?php echo $produto['nome'] ?></div></a>
					<a href="<?php echo base_url('produtos/visualizar/'.$produto['slug']) ?> "><div class="subtitulo-produto-listagem"><?php echo $produto['descricao'] ?></div></a>
				</div>				
			<?php endforeach ?>
			<div class="paginacao"><?php echo $pagination ?></div>
		</div>
	</div>
</div>


<script>
	jQuery(document).ready(function($) {
		$('.linha-categoria').on('click', function(){
			$(this).next().slideToggle('fast');

		})
	$('.subcategoria-ativa').parent().parent().show();
		
	});
	

</script>