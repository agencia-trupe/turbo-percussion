<?php 

class Caso extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }
	function insere_caso($post){
		$this->db->insert('casos', $post['caso']);
		$id_caso = $this->db->insert_id();
		$fotos_caso = array();
		foreach ($post['fotos_caso'] as $k => $v) {
			$fotos_caso[] = array(
					'id_caso' => $id_caso,
					'url' => $v

				);
		}

		$this->db->insert_batch('fotos_caso', $fotos_caso);
		return true;

	}

	function edita_caso($post){
		$id = $post['caso']['id'];
		$this->db->update('casos', $post['caso'], array('id' => $id));
		$this->db->delete('fotos_caso', array('id_caso' => $id));
		$fotos_caso = array();
		foreach ($post['fotos_caso'] as $k => $v) {
			$fotos_caso[] = array(
					'id_caso' => $id,
					'url' => $v

				);
		}

		$this->db->insert_batch('fotos_caso', $fotos_caso);
		return true;
	}
} 


?>