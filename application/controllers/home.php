<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start(); //we need to call PHP's session object to access it through CI
class Home extends CI_Controller {
  
  public $data;

 function __construct()
 {
  
   parent::__construct();

   $this->data['page'] = 'home';
   #$this->data['contato'] = $this->db->get('contato')->row_array();
   $this->data['slides_home'] = $this->db->get('slides_home')->result_array();
   $this->data['parceiros'] = $this->db->get('parceiros')->result_array();
   $this->data['contato'] = $this->db->get('contato_principal')->row_array();

   $this->data['destaques'] = $this->db->order_by('ordem')->get('destaques')->result_array();
   foreach ($this->data['destaques'] as $key => $destaque) {
    $this->data['destaques'][$key]['produto'] = $this->db->get_where('produtos', array('id' => $destaque['id_produto']))->row_array();
     
   }

 }

 function index($slug = false)
 {
   $this->data['title'] = 'Home';
     $this->load->view('layout/template', $this->data);
   }
   
 

 function salva_newsletter()
 {
 	 
     $this->db->insert('newsletter', $this->input->post('newsletter'));
     $this->session->set_flashdata('newsletter_sucesso', 'Seu cadastro no nosso newsletter foi realizado com sucesso!');
    redirect(base_url(''), 'refresh');
   }
   
 }






