<div class="banner banner-onde-comprar"  id='banner'>
	<div class='wrapper-banner'>Vídeos</div>
</div>

<div class="container-corpo">
	<div class="wrapper-corpo">
		<div class="coluna-novidade">
			<div class="foto-novidade " id='!'>
				<iframe id="ytplayer" type="text/html" width="640" height="360"
				src="https://www.youtube.com/embed/<?php echo $video_destaque['vid'] ?>"
				frameborder="0" allowfullscreen></iframe>
			</div>
			<div class="titulo-novidade titulo-novidade-2"><?php echo $video_destaque['titulo'] ?></div>
			<div class="texto-novidade"><?php echo $video_destaque['texto'] ?></div>
			<div class="voltar"><a href="<?php echo base_url('videos') ?>">voltar</a></div>
			<div class="borda-novidade-lateral"></div>
		</div>
		
		<div class="coluna-novidade-2">
		<div class="ultimos-videos">Últimos Vídeos</div>
			<?php foreach ($videos as $k => $video): ?>
				<div data-redirect='<?php echo base_url('videos/'.$video['slug'].'#banner') ?> ' class='thumb-video-2 <?php if ($k == 3): ?> thumb-sem-margem <?php endif ?>'  ><iframe id="ytplayer" type="text/html" width="190" height="96"
				src="https://www.youtube.com/embed/<?php echo $video['vid'] ?>"
				frameborder="0" allowfullscreen></iframe><div data-redirect='<?php echo base_url('videos/'.$video['slug'].'#banner') ?> '  class="video-redirect"></div></div>
				
			<?php endforeach ?>
		</div>
		

	</div>
</div>


<script>
	jQuery(document).ready(function($) {
		$('.video-redirect').on('click', function(){
			window.location = $(this).attr('data-redirect')	;
		})

		$('.coluna-novidade').css('height', $('.wrapper-corpo').height());
	});

	$(window).on('load', function(){
		$('.coluna-novidade').css('height', $('.wrapper-corpo').height());
		
	})
</script>