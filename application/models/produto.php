<?php 

class Produto extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }
	function insere_produto($post){
		
		$this->db->insert('produtos', $post['produtos']);

		$id_produto = $this->db->insert_id();

		$produto_fotos = array();
		if(!empty($post['produto_fotos'])){

			foreach ($post['produto_fotos'] as $k => $v) {
				$produto_fotos[] = array(
						'id_produto' => $id_produto,
						'url' => $v,
						'ordem' => $k
					);
			}
		$this->db->insert_batch('produto_fotos', $produto_fotos);
		}


		$tipo_tabela = $this->input->post('n_colunas');

		$this->db->set('n_colunas', $tipo_tabela)->where('id', $id_produto)->update('produtos');

		if($tipo_tabela == 3){

			$tabela = $this->input->post('3col');

			$var_titulo1 = $tabela['titulo-coluna1']; // texto
			$var_titulo2 = $tabela['titulo-coluna2']; // texto
			$linhas = $tabela['linha']; // array

			$this->db->where('id', $id_produto)
					->set('caracteristica_1', $var_titulo1)
					->set('caracteristica_2', $var_titulo2)
					->update('produtos');

			$contador = 0;
			foreach ($linhas as $key => $value) {

				if($contador == 0)
					$this->db->set('nome', $value);
				if($contador == 1)
					$this->db->set('valor_1', $value);
				if($contador == 2)
					$this->db->set('valor_2', $value);						

				$contador++;

				if($contador==3){
					$this->db->set('id_produto', $id_produto)->insert('produto_caracteristicas');
					$contador = 0;
				}
			}			

		}elseif($tipo_tabela == 2){

			$tabela = $this->input->post('2col');

			$var_titulo1 = $tabela['titulo-coluna1']; // texto
			$linhas = $tabela['linha']; // array

			$this->db->where('id', $id_produto)
					->set('caracteristica_1', $var_titulo1)					
					->update('produtos');

			$contador = 0;
			foreach ($linhas as $key => $value) {

				if($contador == 0)
					$this->db->set('nome', $value);
				if($contador == 1)
					$this->db->set('valor_1', $value);
				
				$contador++;

				if($contador==2){
					$this->db->set('id_produto', $id_produto)->insert('produto_caracteristicas');
					$contador = 0;
				}
			}
		}

		return true;
	}

	function edita_produto($post){

		$id = $post['produtos']['id'];

		$this->db->update('produtos', $post['produtos'], array('id' => $id));
		
		$this->db->delete('produto_fotos', array('id_produto' => $id));
		$this->db->delete('produto_caracteristicas', array('id_produto' => $id));
		$produto_fotos = array();
		if(isset($post['produto_fotos']) && !empty($post['produto_fotos'])){
			foreach ($post['produto_fotos'] as $k => $v) {
				$produto_fotos[] = array(
					'id_produto' => $id,
					'url' => $v,
					'ordem' => $k,
				);
			}

			$this->db->insert_batch('produto_fotos', $produto_fotos);
		}
		
		$tipo_tabela = $this->input->post('n_colunas');

		$this->db->set('n_colunas', $tipo_tabela)->where('id', $id)->update('produtos');

		if($tipo_tabela == 3){

			$tabela = $this->input->post('3col');

			$var_titulo1 = $tabela['titulo-coluna1']; // texto
			$var_titulo2 = $tabela['titulo-coluna2']; // texto
			$linhas = $tabela['linha']; // array

			$this->db->where('id', $id)
					->set('caracteristica_1', $var_titulo1)
					->set('caracteristica_2', $var_titulo2)
					->update('produtos');

			$contador = 0;
			foreach ($linhas as $key => $value) {

				if($contador == 0)
					$this->db->set('nome', $value);
				if($contador == 1)
					$this->db->set('valor_1', $value);
				if($contador == 2)
					$this->db->set('valor_2', $value);						

				$contador++;

				if($contador==3){
					$this->db->set('id_produto', $id)->insert('produto_caracteristicas');
					$contador = 0;
				}
			}			

		}elseif($tipo_tabela == 2){

			$tabela = $this->input->post('2col');

			$var_titulo1 = $tabela['titulo-coluna1']; // texto
			$linhas = $tabela['linha']; // array

			$this->db->where('id', $id)
					->set('caracteristica_1', $var_titulo1)					
					->update('produtos');

			$contador = 0;
			foreach ($linhas as $key => $value) {

				if($contador == 0)
					$this->db->set('nome', $value);
				if($contador == 1)
					$this->db->set('valor_1', $value);
				
				$contador++;

				if($contador==2){
					$this->db->set('id_produto', $id)->insert('produto_caracteristicas');
					$contador = 0;
				}
			}
		}
		
		return true;
	}
  
    private function toAscii($str, $replace=array(), $delimiter='-') {
      if( !empty($replace) ) {
        $str = str_replace((array)$replace, ' ', $str);
      }

      $clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
      $clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
      $clean = strtolower(trim($clean, '-'));
      $clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);

      return $clean;
    }

	function carrega_por_categoria($id){
		$produtos = $this->db->query("SELECT *
			from produtos 
			where produtos.id_categoria = '".$id."'")
		->result();

		foreach ($produtos as $i => $produto) {
			$produtos[$i]->fotos_produto = $this->db->query("SELECT *
			from fotos_produto 
			where fotos_produto.id_produto = '".$produto->id."'")
		->result();
		}
		return $produtos;
	}
} 


?>