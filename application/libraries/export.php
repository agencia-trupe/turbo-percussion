<?php 
if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
* Excel library for Code Igniter applications
* Based on: Derek Allard, Dark Horse Consulting, www.darkhorse.to, April 2006
* Tweaked by: Moving.Paper June 2013
*/
class Export{
    
    function to_excel($array, $filename) {
        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename='.$filename.'.xls');


        $h = array();
        foreach($array->result_array() as $row){
            foreach($row as $key=>$val){
                if(!in_array($key, $h)){
                 $h[] = $key;   
                }
                }
                }
                //echo the entire table headers
                echo '<table><tr>';
                foreach($h as $key) {
                    $key = ucwords($key);
                    echo '<th>'.$key.'</th>';
                }
                echo '</tr>';
                
                foreach($array->result_array() as $row){
                     echo '<tr>';
                    foreach($row as $index => $val)
                        if($index != 'timestamp'){

                         echo '<td>'.utf8_decode($val).'</td>';              
                        }else{
                         echo '<td>'.utf8_decode(date('d/m/Y H:i:s', strtotime($val))).'</td>';              
                            
                        }
                }
                echo '</tr>';
                echo '</table>';
                
            
        }
    function writeRow($val) {

                
    }

}

?>