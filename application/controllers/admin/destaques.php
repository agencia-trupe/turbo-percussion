<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start(); //we need to call PHP's session object to access it through CI
class Destaques extends CI_Controller {

    function __construct(){
        parent::__construct();
        $this->objeto       = 'Destaque';
        $this->objetos      = 'Destaques';
        $this->slug         = 'destaques';
        $this->genero       = 'o';
        $this->data['attr'] = array(
            'objeto' => $this->objeto,
            'objetos' => $this->objetos,
            'slug' => $this->slug,
            'genero' => $this->genero

        );
    }

    function index()  {
        if ($this->session->userdata('logged_in'))    {

                $session_data = $this->session->userdata('logged_in');
                $this->data['title'] = $this->objeto;
                $this->data[$this->slug] = $this->db->order_by('ordem')->get($this->slug)->result();
                foreach ($this->data[$this->slug] as $key => $v) {
                  $this->data[$this->slug][$key]->produto = $this->db->get_where('produtos', array('id'=> $v->id_produto))->row();
                  
                }

                $this->load->view('admin/template', $this->data);
        } else {
        redirect('admin/login', 'refresh');
        }
    }

    function editar($id)  {
        if ($this->session->userdata('logged_in'))    {

            if(isSet($_POST) && $_POST){
                 $this->db->update($this->slug, $this->input->post($this->slug), array('id' => $id));
                 $this->session->set_flashdata('message', $this->objeto.' editad'.$this->genero .' com sucesso!');
                 redirect('admin/'.$this->slug, 'refresh');
            }
                $session_data = $this->session->userdata('logged_in');
                $this->data['title'] = $this->objeto;
                $this->data[$this->slug] = $this->db->get_where($this->slug, array('id' => $id))->row();

                $this->load->view('admin/template', $this->data);
        } else {
        redirect('admin/login', 'refresh');
        }
    }

    function inserir()  {
        if ($this->session->userdata('logged_in'))    {

            if(isSet($_POST) && $_POST){
                $this->db->insert($this->slug, $this->input->post($this->slug));
                $this->session->set_flashdata('message', $this->objeto.' inserid'.$this->genero .' com sucesso!');
                redirect('admin/'.$this->slug, 'refresh');
            }
                $session_data = $this->session->userdata('logged_in');
                $this->data['title'] = $this->objeto;
                $this->data['produtos'] = $this->db->get('produtos')->result();
                $this->data[$this->slug] = '';

                $this->load->view('admin/template', $this->data);
        } else {
        redirect('admin/login', 'refresh');
        }
    }


  function deletar($id){

    if($this->session->userdata('logged_in'))  {

      $session_data = $this->session->userdata('logged_in');


      $this->db->delete($this->slug, array('id' => $id));

      $this->session->set_flashdata('message', $this->objeto." deletad".$this->genero." com sucesso!");
      redirect('admin/'.$this->slug, 'refresh');

    } else {
    //If no session, redirect to login page
    redirect('admin/login', 'refresh');
    }
  }


  function grava_ordem(){
    if($this->session->userdata('logged_in')) {
      $post = $this->input->post();
      $textos_banner = $this->db->get($this->slug)->result();

      foreach ($post['item'] as $k => $v) {
      $this->db->update($this->slug, array('ordem' => ++$k), array('id' => $v));
      }

    }  else  {
    //If no session, redirect to login page
    redirect('admin/login', 'refresh');
    }

  }





   function upload_foto($id = ''){
      
      $config['upload_path'] = "./assets/img/uploads/".$this->slug;
      $config['allowed_types'] = 'gif|jpg|png|doc|txt';
      $config['max_size']  = 0;
      $config['encrypt_name'] = TRUE;
 
      $file_element_name = 'file';
      $this->load->library('upload', $config);
  
      if (!$this->upload->do_upload($file_element_name))
      {
         $status = 'error';
         $msg = $this->upload->display_errors('', '').$config['upload_path'];
         $data['file_name'] = '';
      }
      else
      {
         $data = $this->upload->data();
      
         if($data['file_name'])
         {
            $status = "success";
            $msg = "File successfully uploaded";
         }
         else
         {
            unlink($data['full_path']);
            $status = "error";
            $msg = "Something went wrong when saving the file, please try again.";
         }

         $this->load->library('image_moo');
         $this->image_moo->load($data['full_path'])->set_jpeg_quality(100)->resize_crop(380,465)->save($data['full_path'], TRUE);
         if ($this->image_moo->errors) print $this->image_moo->display_errors();


      }

      @unlink($_FILES[$file_element_name]);
         echo json_encode(array('status' => $status, 'msg' => $msg, 'url' => base_url().'assets/img/uploads/'.$this->slug.'/'.$data['file_name'], 'filename' => $data['file_name']));

 }







}
