
<%
'-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#
' Loja Exemplo Locaweb
' Versao: 6.5
' Data: 12/09/06
' Arquivo: cabecalho.asp
' Versao do arquivo: 0.0
' Data da ultima atualiza�ao: 04/04/08
'
'-----------------------------------------------------------------------------
' Licen�a C�digo Livre: http://comercio.Locaweb.com.br/gpl/gpl.txt
'-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
%>
<!--#INCLUDE FILE="funcoes/funcoes.asp" -->
<!--#INCLUDE FILE="funcoes/funcoes_valida.asp" -->
<%
'Caso a p�gina corrente nao seja o carrinho de compras � feito a checagem de caracteres.
If page <> "carrinho" Then
    'Verifica a existencia de caracteres inv�lidos inclui neste caso palavras como UPDATE/DELETE/SELECT/ETC...
    Call Valida_Request()
Else
    'Verifica a existencia de caracteres inv�lidos, apenas caracteres.
    Call Valida_RequestCarrinho()
End if

'Carrega a configura�ao dos caminhos a serem usuados na Loja
Call identifica_caminhos()

If request("lang") <> "" Then
    Session("requestIdioma") = request("lang")
    varLang = Session("requestIdioma")
End If

'Caso exista um cookie de algum pedido anterior o id_transacao ser� recriado a partir dele.
If session("id_transacao") = "" And Request.Cookies(Application("nomeConfiguracao"))("id_transacao") <> "" Then
    session("id_transacao") = Request.Cookies(Application("nomeConfiguracao"))("id_transacao")
End if

'Se existir a id_transacao o idioma de exibi�ao ser� capturado do arquivo do pedido
If session("id_transacao") <> "" Then
    If VerificaExistenciaArquivo(Application("DiretorioPedidos")&session("id_transacao")&".xml") Then
        varLangUser = pegaValorAtrib(Application("DiretorioPedidos")&session("id_transacao")&".xml","dados_pedido","siglaidioma")
        session("varLangUser") = varLangUser
    Else
        session("id_transacao") = ""
        session("varLangUser") = ""
    End If
End If

If varLangUser <> "" Then
    varLang = varLangUser
Else
    varLang = Application("varLang")
End if

varSkin = "default"

'Assume a string dos idiomas configurados
checkIdioma =  Application("idiomasconfigurados")
veIdiomas = Split(checkIdioma, ",") 
%>
<!--#INCLUDE FILE="funcoes/funcoes_config_loja.asp" -->
<%
'Abre conexao com o Banco de dados
Call abre_conexao(conexao)
'Carrega as applications com os textos configur�veis
Server.execute("config/templates/"&varLang&"/textos_lang.asp")

' Monta a URL da loja
If page = "recibo" Then 'Para a p�gina de recibo
    sURLloja = Application("URLloja") & "/carrinho_vazio.asp?redirect=default"
Else 'Para as demais p�ginas
    sURLloja = Application("URLloja") & "?lang=" & varLang
End If
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" 
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="pt-BR" xml:lang="pt-BR" xmlns="http://www.w3.org/1999/xhtml">
<!-- The above DOCTYPE and html lines and the meta http-equiv line below the head are critical. DO NOT DELETE them -->
<head>
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
    <title><%=Application("nomeloja")%></title>
    <script type="text/javascript"  src="config/templates/<%=varLang%>/textos_JS.js"></script>
    <script type="text/javascript"  src="funcoes/funcoes_js.js"></script>
    <script type="text/javascript" src="js/swfobject.js"></script>

<link rel="stylesheet" href="config/css.css" type="text/css" />
<link rel="stylesheet" href="css/layout.css" type="text/css" />
<link rel="stylesheet" href="config/rssnews.css" type="text/css" />
<link rel="stylesheet" href="sIFR-screen.css" type="text/css" media="screen" />

<script src="sifr.js" type="text/javascript"></script>
<script src="sifr-addons.js" type="text/javascript"></script>

</head>
<body leftmargin="0" topmargin="0">
<table id="container" align="center" height="100%" width="960px" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td colspan="4" valign="top" height="30">
<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF">
    <tr>
        <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="TBLcabecalho">
                  <tr>
    <td colspan="2" align="center" height="230">
<!--#include file="menutopo.asp" -->
    
    </td>
  </tr>
  <tr>
    <td height="240" colspan="2" align="center">
          	  	  	<div id="flashcontent">
            		<strong>Este conte&uacute;do utiliza o plugin do Flash 8.</strong>
                    Atualize seu browser.</div>
	<script type="text/javascript">
		// <![CDATA[
		
		var so = new SWFObject("flash/banners.swf", "header", "925", "230", "8");
		so.addParam("wmode", "transparent");
		so.write("flashcontent");
	
		// ]]>
	</script>
	</td>
  </tr>

            </table>
      </td>
    </tr>
    <tr>
        <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
			    	<td width="12px"><img src="config/templates/<%=varLang%>/<%=varSkin%>/regua1x1.gif" width="12" /></td>
                    <td class="MNdivisorlatesquerda" width="154"><%If navegacaocompra = "fim" Then%>
                      <img src="config/imagens_conteudo/padrao/tit_busca.gif" width="111" height="22" />
                      <%Else %>
                  <img src="config/imagens_conteudo/padrao/tit_busca.gif" width="111" height="22" />
                  <%End if%></td>
                    <td bgcolor="#FFFFFF" width="1"><img src="config/templates/<%=varLang%>/<%=varSkin%>/regua1x1.gif" width="1" /></td>
                    <td align="right" class="MNdivisorcabecalho" style="padding-top:2px;padding-bottom:2px;padding-left:10px;padding-right:10px">&nbsp;</td>
                </tr>
            </table>
      </td>
    </tr>
</table>