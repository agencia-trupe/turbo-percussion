<div class="banner banner-produtos">
	<div class='wrapper-banner'>Contato</div>
</div>

<div class="container-corpo">
	<div class="wrapper-corpo">
		
		<form action="<?=base_url('contato/enviar')?>" method="post" id='form-contato'>

			<div class="coluna-manutencao-1">

				<?php if ($this->session->flashdata('enviocontato')): ?>
					<div class="mensagem-envio-contato">
						Sua mensagem foi enviada com sucesso!<br>
						Entraremos em contato assim que possível.
					</div>
				<?php endif ?>

				<div class="linha-form">
					<label for="nome" class='label-form-1'>Nome</label>
					<input type="text" class='input-form-1' name="nome" required data-msg="Informe seu nome">
				</div>

				<div class="linha-form">
					<label for="email" class='label-form-1'>Email</label>
					<input type="text" class='input-form-1' name="email" required data-msg="Informe seu e-mail">
				</div>
				
				<div class="linha-form">
					<label for="assunto" class='label-form-1'>Assunto</label>
					<input type="text" class='input-form-1' name='assunto' required data-msg="Informe o assunto">
				</div>

				<div class="linha-form">
					<label for="mensagem" class='label-form-1'>Mensagem</label>
					<textarea type="text" class='textarea-form-1' name='mensagem' required data-msg="Informe sua mensagem"></textarea>
				</div>				

				<div class="linha-form">
					<input type='submit' class="botao-enviar-contato" value='enviar'>
				</div>
			</div>

			<div class="coluna-manutencao-1">
				
				<div id="container-produtos">
					<div class="linha-form">
						<label for="nome" class='label-form-1 label-titulo'>Nossos Representantes</label>				
					</div>

					<div class="linha-form">
						<label for="nome" class='label-form-3'>Escolha a cidade</label>
					
						<div class="wrapper-select-3">			
							<select id='select-contatos' class='input-form-3'>
								<option disabled selected> selecione...</option>
								<?php foreach ($contatos_cidade as $key => $v): ?>
									<option value="<?php echo $v->id ?>"> <?php echo $v->nome ?> </option>
								<?php endforeach ?>
					 		</select>
						</div>
					</div>
				</div>
			
				<div class="linha-form">				
					<div class="texto-contato"></div>
				</div>
			</div>

		</form>
	</div>
	
</div>

<script>
	jQuery(document).ready(function($) {

		$('#select-contatos').on('change', function(){
			console.log($(this).val());
			texto = [];
			<?php foreach ($contatos_cidade as $key => $v) : ?>
				<?php 
				$texto = '';
				foreach ($v->contato as $key => $contato) {
					$texto .= "$contato->nome<br/>";
					$texto .= ($contato->telefone) ? "Telefone: $contato->telefone<br/>" : '';
					$texto .= ($contato->celular) ? "Celular: $contato->celular<br/>" : '';
					$texto .= ($contato->nextel) ? "ID Nextel: $contato->nextel<br/>" : '';
					$texto .= ($contato->email) ? "Email : $contato->email<br/>" : '';
					$texto .= ($contato->email_2) ? " $contato->email_2<br/>" : '';
					$texto .= "<br/>";
				}
				?>

				texto[<?php echo $v->id ?>] = "<?php echo $texto ?>";			 
			<?php endforeach;  ?>
			$('.texto-contato').html(texto[$(this).val()]);
		});

		$('#form-contato').submit( function(e){
			$('[required]').each( function(){
				if($(this).val() == ''){
					alert($(this).attr('data-msg'));
					$(this).focus();
					e.preventDefault();
					return false;
				}
			});
		});

		if($('.mensagem-envio-contato').length){
			setTimeout( function(){
				$('.mensagem-envio-contato').addClass('removido');
			}, 4000);
		}
	});
</script>
