<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
    <meta name="viewport" content="width=1024, initial-scale=1">
    <meta name='description' content="Há 17 anos no mercado, a TURBO decida-se à importação e distribuição de Instrumentos Musicais de qualidade a um preço justo."/>
    <meta name='keywords' content="Instrumentos musicais, baterias, baterias junior, guitarras, guitarras junior, baixo teenager, microfone JTS com fio, microfone JTS sem fio, Violões"/>

    <?php if(isset($seo) && $seo): ?>
        <meta property="og:image" content="<?php echo base_url($seo['imagem']) ?>"/>
        <meta property="og:title" content="Turbo Percussion<?php echo " - ".$seo['titulo']?>"/>
    <?php else: ?>
        <meta property="og:image" content="<?php echo base_url('assets/img/logo.png') ?>"/>
        <meta property="og:title" content="Turbo Percussion<?php echo " - ".$title?>"/>
    <?php endif; ?>

    <meta property="og:url" content="<?php echo current_url() ?>"/>
    <meta property="og:site_name" content="Turbo Percussion"/>
    <meta property="og:type" content="website"/>
    <meta property="og:description" content="Há 17 anos no mercado, a TURBO decida-se à importação e distribuição de Instrumentos Musicais de qualidade a um preço justo. "/>
    <meta property="fb:admins" content="100002297057504"/>
    <meta property="fb:app_id" content="599379900117848">

    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/main.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/colorbox.css">
    <!-- <script src="<?php echo base_url();?>assets/js/less.js"   type="text/javascript"></script> -->
    <script src="<?php echo base_url(); ?>assets/js/modernizr-2.6.2.min.js"></script>

    <?php if ($this->uri->uri_string() == 'reservas' || $this->uri->uri_string() == 'cotacao_online'): ?>
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/js/jqueryui/jquery-ui-1.10.3.custom.min.css">
        <script src="<?php echo base_url(); ?>assets/js/jqueryui/jquery-ui-1.10.3.custom.min.js"   type="text/javascript"></script>
    <?php else: ?>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>      
    <?php endif ?>
    
    <script src="<?php echo base_url(); ?>assets/js/cycle.js"   type="text/javascript"></script>

    <title>Turbo Percussion<?php echo " - ".$title?></title>
    
</head>


<body>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/pt_BR/all.js#xfbml=1&appId=599379900117848";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div class="page">
    

</div>    
<header>
    <div class="container-header">
        <a href="<?php echo $contato['facebook'] ?>"><img  class="facebook-home icone-header" src='<?php echo base_url('assets/img/icone-facebook.png') ?>' data-hover=' <?php echo base_url('assets/img/icone-facebook-hover.png') ?>' data-url='<?php echo base_url('assets/img/icone-facebook.png') ?>'  >        </a>
        <a href="<?php echo $contato['twitter'] ?>   "><img  class="twitter-home icone-header" src='<?php echo base_url('assets/img/icone-twitter.png') ?>' data-hover=' <?php echo base_url('assets/img/icone-twitter-hover.png') ?>' data-url='<?php echo base_url('assets/img/icone-twitter.png') ?>' alt='' >        </a>
        <a href="<?php echo $contato['youtube'] ?>  "><img  class="youtube-home icone-header" src='<?php echo base_url('assets/img/icone-youtube.png') ?>' data-hover=' <?php echo base_url('assets/img/icone-youtube-hover.png') ?>' data-url='<?php echo base_url('assets/img/icone-youtube.png') ?>' alt='' >        </a>
        
        <form class='form-pesquisa-produto'> 

            <div class="pesquisa-home icone-header"><input id='query-pesquisa' type="text" class='input-invisivel input-1' placeholder="Busca"><img class='icone-pesquisa' src='<?php echo base_url('assets/img/icone-busca.png') ?>' alt='' ></div>        

        </form>


        <div class="clear"></div>
        <div class="nav-bar">
            <ul class="lista-nav-itens">
                <a href="<?php echo base_url('empresa') ?>"><li class="nav-item <?php if ($this->uri->segment(1) == 'empresa'): ?> active        <?php endif ?>">Empresa</li></a>
                <a href="<?php echo base_url('produtos') ?>"><li class="nav-item <?php if ($this->uri->segment(1) == 'produtos'): ?> active        <?php endif ?>">Produtos</li></a>
                <a href="<?php echo base_url('novidades') ?>"><li class="nav-item <?php if ($this->uri->segment(1) == 'novidades'): ?> active        <?php endif ?>">Novidades</li></a>
                <a href="<?php echo base_url('onde_comprar') ?>"><li class="nav-item <?php if ($this->uri->segment(1) == 'onde_comprar'): ?> active        <?php endif ?>">Onde Comprar</li></a>
                <a href="<?php echo base_url('') ?> "><li class="meio-nav-bar"><img class='logo-menu' src='<?php echo base_url('assets/img/logo.png') ?>' alt='' ></li></a>
                <a href="<?php echo base_url('manutencao') ?>"><li class="nav-item <?php if ($this->uri->segment(1) == 'manutencao'): ?> active        <?php endif ?>">Manutenção</li></a>
                <a href="<?php echo base_url('videos') ?>"><li class="nav-item <?php if ($this->uri->segment(1) == 'videos'): ?> active        <?php endif ?>">Vídeos</li></a>
                <a href="http://200.150.179.88:8080/TurboRV/sgw0001.do?method=login"><li class="nav-item <?php if ($this->uri->segment(1) == 'representantes'): ?> active        <?php endif ?>">Representantes</li></a>
                <a href="<?php echo base_url('contato') ?>"><li class="nav-item <?php if ($this->uri->segment(1) == 'contato'): ?> active        <?php endif ?>">Contato</li></a>
            </ul>

        </div>

    </div>
</header>   
        