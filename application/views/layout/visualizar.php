<div class="banner banner-produtos	">
	<div class='wrapper-banner'>Produtos</div>
</div>

<div class="container-corpo">
	<div class="wrapper-corpo">
		<div class="coluna-categorias">
			<div class="titulo-categorias">Categorias</div>
			<?php foreach ($produto_categorias as $key => $produto_categoria): ?>
				<div class="linha-categoria"><span><?php echo $produto_categoria['titulo'] ?></span>
				</div>
				<div class="subcategorias">
					<?php foreach ($produto_categoria['produto_subcategorias'] as $k => $v): ?>
						<a href="<?php echo base_url('produtos/subcategoria/'.$v['slug']) ?>"><div class="linha-subcategoria <?php if ($v['id'] == $produto['id_subcategoria']): ?> subcategoria-ativa <?php endif ?>"><?php echo $v['titulo'] ?></div></a>
					<?php endforeach ?>
				</div>
			<?php endforeach ?>
		</div>
		<div class="coluna-produto">
			<div class="titulo-produto"><?php echo $produto['nome'] ?></div>
			<img class='foto-produto' src='<?php echo base_url('assets/img/uploads/produto_fotos/'.$produto['imagem_principal']) ?>' alt='' >
			<div class="container-miniaturas">
			<img src='<?php echo base_url('assets/img/uploads/produto_fotos/'.$produto['imagem_principal'].'_thumb.jpg') ?>'  class='miniatura-produto' alt='' >
				<?php foreach ($produto['fotos_produto'] as $key => $v): ?>
					<img src='<?php echo base_url('assets/img/uploads/produto_fotos/'.$v['url'].'_thumb.jpg') ?>'  class='miniatura-produto' alt='' >
				<?php endforeach ?>

			</div>
			
			<div class="clear"></div>
			<div class="fb-like" style="margin:20px 0 0 0;" data-href="<?php echo current_url() ?>" data-layout="button_count" data-action="like" data-show-faces="false" data-share="true"></div>
			<div class="informacoes">Informações</div>
			<div class="informacoes-produto"><div class='descricao-produto'><?php echo $produto['descricao'] ?></div class='descricao-produto'> <?php echo $produto['informacoes'] ?></div>
			

			<?php if (!empty($produto['url_manual'])): ?>
				<a href="<?php echo base_url('produtos/download/'.$produto['id'].'/produto_manuais') ?>"><img class='download-manual' data-hover='<?php echo base_url('assets/img/h-download-manual.jpg') ?>' data-url='<?php echo base_url('assets/img/download-manual.png') ?>' src='<?php echo base_url('assets/img/download-manual.png') ?>' alt='' ></a>
			<?php endif ?>
			<div class="clear"></div>
			
			<?php if (sizeof($produto['caracteristicas_produto'])): ?>

				<div class="informacoes">Características</div>

				<div class="caracteristicas">

					<?php if ($produto['n_colunas'] == 2): ?>

						<div class="duas-colunas">
							<div class="celula-caracteristica caracteristica-clara"></div>
							<div class="celula-caracteristica caracteristica-clara"><div><?php echo $produto['caracteristica_1'] ?></div></div>

							<?php foreach ($produto['caracteristicas_produto'] as $key => $v): ?>
								<?php $classe = (!($key%2))  ? 'caracteristica-escura' : 'caracteristica-clara'; ?>
								<div class="celula-caracteristica <?php echo $classe ?>"><div><?php echo $v['nome'] ?></div></div>
								<div class="celula-caracteristica <?php echo $classe ?>"><div><?php echo $v['valor_1'] ?></div></div>							
							<?php endforeach ?>
						</div>
					<?php elseif($produto['n_colunas'] == 3): ?>
		
						<div class="celula-caracteristica caracteristica-clara"></div>
						<div class="celula-caracteristica caracteristica-clara"><div><?php echo $produto['caracteristica_1'] ?></div></div>
						<div class="celula-caracteristica caracteristica-clara"><div><?php echo $produto['caracteristica_2'] ?></div></div>

						<?php foreach ($produto['caracteristicas_produto'] as $key => $v): ?>
							<?php $classe = (!($key%2))  ? 'caracteristica-escura' : 'caracteristica-clara'; ?>
							<div class="celula-caracteristica <?php echo $classe ?>"><div><?php echo $v['nome'] ?></div></div>
							<div class="celula-caracteristica <?php echo $classe ?>"><div><?php echo $v['valor_1'] ?></div></div>
							<div class="celula-caracteristica <?php echo $classe ?>"><div><?php echo $v['valor_2'] ?></div></div>
						<?php endforeach ?>

					<?php endif ?>
					
				</div>
					
			<?php endif ?>

			<div class="voltar voltar-produto"><a href="javascript:history.back(-1)">voltar</a></div>
		</div>
	</div>
</div>


<script>

	jQuery(document).ready(function($) {
		$('.linha-categoria').on('click', function(){
			$(this).next().slideToggle('fast');
		})

	$('.miniatura-produto').on('click', function(){
		url = $(this).attr('src').replace('_thumb.jpg', '');
		$('.foto-produto').attr('src', url);

	})
	$('.subcategoria-ativa').parent().parent().show();
	

	$('.download-manual').hover(function(){
		$(this).attr('src', $(this).attr('data-hover'))
	}, function(){
		$(this).attr('src', $(this).attr('data-url'))
		
	})


});


</script>