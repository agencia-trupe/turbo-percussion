<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.css">
    <title>&Aacute;rea Administrativa - Turbo Percussion</title>
  </head>
  <body>
  <style>
    .quadrado_login{width:300px; height:300px; background-color: #ddd; margin-left:40%;margin-top:50px;  }
    label{text-align:right;}
    #titulo_admin{text-align: center; color:#46B8DA;}
  </style>
  <div class="quadrado_login">
  <?php if (validation_errors()): ?>
    
  <div class="alert alert-error"> 
    Usu&aacute;rio e/ou senha incorretos
   </div>
  <?php endif ?>
    <?php echo form_open('admin/verifylogin'); ?>
    <div class="row-fluid">
      <div class="col-12">
        <legend id='titulo_admin'><h1>Turbo Percussion</h1></legend>
      </div>
    </div>
    <div class="row-fluid">
      <div class="col-3 text-right">
        <label for="username" class='control-label'>Usu&aacute;rio:</label>

      </div>
      <div class="col-9">
        <input type="text" size="20" id="username" class='form-control' name="username"/>

      </div>
    </div>
    <div class="clearfix"></div>

    <div class="row-fluid">
      <div class="col-3 text-right">
       <label for='password' class='control-label'>Senha:</label>

      </div>


      <div class="col-9">
       <input type="password" size="20" class='form-control' id="password" name="password"/>


      </div>
    </div>
    <div class="clearfix"></div>
      <div class="row-fluid">
        <div class="col-3">
          
        </div>
        <div class="col-3">
          <button type="submit" class='btn btn-sm btn-info'>Enviar</button>
        </div>
        
      </div>

    </div>
  </div>
 
    
  </form>
  <script>

  </script>

  </body>
</html>

