<div class="row-fluid">

	<div class="col-lg-12"><?php if ($this->session->flashdata('message')): ?>
		<div class="alert alert-success"> <?php echo $this->session->flashdata('message') ?> </div>
		
	<?php endif ?>
	</div>
</div>
<div class="row-fluid">
	<div class="col-12-">
		<legend>Inserir Usuário</legend>
	</div>
</div>
<form action="<?php echo base_url('admin/usuarios/inserir') ?> " method='POST'>
<div class="row-fluid">
	<div class="col-2"><label class='control-label' for="">Nome:</label> </div>
	<div class="col-2"><input class='form-control' type="text" id='nome' name='user[username]'></div>
</div>
<div class="clearfix"></div>

<div class="row-fluid">
	<div class="col-2"><label class='control-label' for="">Senha:</label> </div>
	<div class="col-2"><input class='form-control' type='password' id='pass1' name='user[password]'></div>
</div>
<div class="clearfix"></div>

<div class="row-fluid">
	<div class="col-2"><label class='control-label' for="">Confirma senha:</label> </div>
	<div class="col-2"><input class='form-control' type="password" id='pass2' ></div>
	<div class="col-3"><div class="error_div"></div></div>

</div>
<div class="clearfix"></div>

<div class="row-fluid">
	<div class="col-4">
		<button id='submit' type='submit' class="btn btn-success btn-sm">Salvar</button>
		<button class="btn btn-info btn-sm" type='button' onclick="history.back(-1)">Voltar</button>
	</div>
</div>
</form>
<script>
	jQuery(function(){
        $("#submit").click(function(){

        $(".alert-warning").hide();

        var hasError = false;
        var passwordVal = $("#pass1").val();
        var checkVal = $("#pass2").val();
        if (passwordVal == '') {
            $(".error_div").append('<span class="alert alert-warning">Por favor, digite uma senha.</span>');
            hasError = true;
        
        } else if (checkVal == '') {
            $(".error_div").append('<span class="alert alert-warning">Digite novamente sua senha.</span>');
            hasError = true;
        
        } else if (passwordVal != checkVal ) {
            $(".error_div").append('<span class="alert alert-warning">As senhas não conferem.</span>');
            hasError = true;
            
        }
        if(hasError == true) {return false;}
        return true;
    });
});
</script>