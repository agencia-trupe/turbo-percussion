<div class="row-fluid">
	<div class="col-lg-12"><?php if ($this->session->flashdata('message')): ?>
		<div class="alert alert-success"> <?php echo $this->session->flashdata('message') ?> </div>
		
	<?php endif ?>
	</div>
</div>

<div class="row-fluid">
	<legend><?php echo $title ?></legend>
</div>
<div class="clearfix"></div>

<form enctype="multipart/form-data"	action="<?php echo base_url('admin/'.$attr['slug'].'/'.$this->uri->segment(3)) ?>" method='post'>
	<div class="row-fluid">
		<div class="col-1 text-right">
			<label for="nome" class='control-label'>Nome</label>
		</div>
		<div class="col-3">
			<input class='form-control' type="text" name='<?php echo $attr['slug'] ?>[nome]' value='<?php echo ($$attr['slug']) ? $$attr['slug']->nome : '' ?>' id='nome'>	
		</div>
	</div><div class="clearfix"></div>


	<div class="row-fluid">
		<div class="col-1 text-right">
			<label for="url" class='control-label'>Manual</label>
		</div>
		<div class="col-3">
			<input class='form-control' type="file" name='userfile' value='<?php echo ($$attr['slug']) ? $$attr['slug']->url : '' ?>' id='url'>	<input type="hidden" name='manuais[url]'  value='<?php echo ($$attr['slug']->url) ? $$attr['slug']->url : '' ?>'>
		</div>
	</div><div class="clearfix"></div>


	<div class="row-fluid">
		<div class="col-1 ">
			
		</div>
		<div class="col-2">
			<button type='submit' class='btn btn-sm btn-success'>Salvar</button>
			<button type='button' onClick='history.back()' class='btn btn-sm btn-info'>Voltar</button>
		</div>
	</div><div class="clearfix"></div>



</form>





