
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start(); //we need to call PHP's session object to access it through CI
class Downloads extends CI_Controller {
  
  public $data;

 function __construct()
 {
  
   parent::__construct();

   $this->data['page'] = 'downloads';
   $this->data['contato'] = $this->db->get('contato_principal')->row_array();
  $this->data['parceiros'] = $this->db->get('parceiros')->result_array();
   $this->data['manuais'] = $this->db->get('manuais')->result();
   
 }

 function index($slug = false)
 {


 	 $divisor = count($this->data['manuais']) / 2;

 	 $this->data['manuais_1'] = array();
 	 $this->data['manuais_2'] = array();
 	 foreach($this->data['manuais'] as $k => $v){
	 	if ($k < $divisor) {	
	 		$this->data['manuais_1'][] = $v;
		}
		else{
			$this->data['manuais_2'][] = $v;
		}
	}
     $this->data['title'] = 'Downloads';
     $this->load->view('layout/template', $this->data);

   }

   // Diretórios possíveis : endorses, manuais, novidades, parceiros, produto_fotos, produto_manuais, produtos, slides_home
   function download($id, $dir){
      $this->load->helper('download');
   		$file = $this->db->get_where('manuais', array('id' => $id))->row();
      // $ch = curl_init(); 
      // curl_setopt($ch, CURLOPT_URL, base_url('assets/img/uploads/'.$dir.'/'.$file->url)); 
      // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
      // $data = curl_exec($ch);
      //  curl_close($ch);
      $data = file_get_contents('assets/img/uploads/'.$dir.'/'.$file->url);
      
      force_download($file->url, $data);

   }
   
 }



?>

