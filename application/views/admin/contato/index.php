<div class="row-fluid">
	<div class="col-lg-12"><?php if ($this->session->flashdata('message')): ?>
		<div class="alert alert-success"> <?php echo $this->session->flashdata('message') ?> </div>
		
	<?php endif ?>
	</div>
</div>

<div class="row-fluid">
	<legend><?php echo $title ?></legend>
</div>
<div class="clearfix"></div>

<form action="<?php echo base_url('admin/'.$attr['slug']) ?>" method='post'>

	<div class="clearfix"></div>
	<div class="row-fluid">
		<div class="col-1 text-right">
			<label for="endereco" class='control-label'>Endereço</label>
		</div>
		<div class="col-5">
			<input class='form-control' type="text" name='<?php echo $attr['slug'] ?>[endereco]' value="<?php echo $$attr['slug']->endereco ?>" id='endereco'>	
		</div>
	</div><div class="clearfix"></div>


	<div class="row-fluid">
		<div class="col-1 text-right">
			<label for="bairro" class='control-label'>Bairro</label>
		</div>
		<div class="col-5">
			<input class='form-control' type="text" name='<?php echo $attr['slug'] ?>[bairro]' value='<?php echo $$attr['slug']->bairro ?>' id='bairro'>	
		</div>
	</div><div class="clearfix"></div>


	<div class="row-fluid">
		<div class="col-1 text-right">
			<label for="cidade" class='control-label'>Cidade</label>
		</div>
		<div class="col-5">
			<input class='form-control' type="text" name='<?php echo $attr['slug'] ?>[cidade]' value='<?php echo $$attr['slug']->cidade ?>' id='cidade'>	
		</div>
	</div><div class="clearfix"></div>


	<div class="row-fluid">
		<div class="col-1 text-right">
			<label for="uf" class='control-label'>UF</label>
		</div>
		<div class="col-1">
			<input class='form-control' type="text" name='<?php echo $attr['slug'] ?>[uf]' value='<?php echo $$attr['slug']->uf ?>' id='uf'>	
		</div>
	</div><div class="clearfix"></div>



	<div class="row-fluid">
		<div class="col-1 text-right">
			<label for="cep" class='control-label'>CEP</label>
		</div>
		<div class="col-5">
			<input class='form-control' type="text" name='<?php echo $attr['slug'] ?>[cep]'  value='<?php echo $$attr['slug']->cep ?>' id='cep'>	
		</div>
	</div><div class="clearfix"></div>





	<div class="row-fluid">
		<div class="col-1 text-right">
			<label for="telefone1" class='control-label'>Tel. 1</label>
		</div>
		<div class="col-5">
			<input class='form-control' type="text" name='<?php echo $attr['slug'] ?>[telefone1]'  value='<?php echo $$attr['slug']->telefone1 ?>'  id='telefone1'>	
		</div>
	</div><div class="clearfix"></div>



	<div class="row-fluid">
		<div class="col-1 text-right">
			<label for="telefone2" class='control-label'>Tel. 2</label>
		</div>
		<div class="col-5">
			<input class='form-control' type="text" name='<?php echo $attr['slug'] ?>[telefone2]' value='<?php echo $$attr['slug']->telefone2 ?>'  id='telefone2'>	
		</div>
	</div><div class="clearfix"></div>


	<div class="row-fluid">
		<div class="col-1 text-right">
			<label for="email" class='control-label'>Email</label>
		</div>
		<div class="col-5">
			<input class='form-control' type="text" name='<?php echo $attr['slug'] ?>[email]'  value='<?php echo $$attr['slug']->email ?>' id='email'>	
		</div>
	</div><div class="clearfix"></div>



	<div class="row-fluid">
		<div class="col-1 ">
			
		</div>
		<div class="col-2">
			<button type='submit' class='btn btn-sm btn-success'>Salvar</button>
			<button type='button' onClick='history.back()' class='btn btn-sm btn-info'>Voltar</button>
		</div>
	</div><div class="clearfix"></div>



</form>