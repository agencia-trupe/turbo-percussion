<div class="row-fluid">
	<div class="col-lg-12"><?php if ($this->session->flashdata('message')): ?>
		<div class="alert alert-success"> <?php echo $this->session->flashdata('message') ?> </div>
		
	<?php endif ?>
	</div>
</div>

<div class="row-fluid">
	<legend><?php echo $title ?></legend>
</div>
<div class="clearfix"></div>

<form action="<?php echo base_url('admin/'.$attr['slug'].'/'.$this->uri->segment(3).'/'.$$attr['slug']->id) ?>" method='post'>


	<div class="row-fluid">
		<div class="col-1 text-right">
			<label for="nome" class='control-label'>Nome</label>
		</div>
		<div class="col-3">
			<input class='form-control' type="text" name='<?php echo $attr['slug'] ?>[nome]' value='<?php echo ($$attr['slug']) ? $$attr['slug']->nome : '' ?>' id='nome'>	
		</div>
	</div><div class="clearfix"></div>


	<div class="row-fluid">
		<div class="col-1 text-right">
			<label for="id_contato_cidade" class='control-label'>Cidade</label>
		</div>
		<div class="col-3">
			<select class='form-control' type="text" name='<?php echo $attr['slug'] ?>[id_contato_cidade]' id='id_contato_cidade'>	
				<option value="" disabled selected></option>
			<?php foreach ($contatos_cidade as $key => $cidade): ?>
				<option value="<?php echo $cidade->id ?>"  <?php if ($cidade->id == $$attr['slug']->id_contato_cidade): ?> selected=selected	<?php endif ?>><?php echo $cidade->nome ?></option>
			<?php endforeach ?>
			</select>
		</div>
	</div><div class="clearfix"></div>

	<div class="row-fluid">
		<div class="col-1 text-right">
			<label for="telefone" class='control-label'>Telefone</label>
		</div>
		<div class="col-3">
			<input class='form-control' type="text" name='<?php echo $attr['slug'] ?>[telefone]' value='<?php echo ($$attr['slug']) ? $$attr['slug']->telefone : '' ?>' id='telefone'>	
		</div>
	</div><div class="clearfix"></div>


	<div class="row-fluid">
		<div class="col-1 text-right">
			<label for="celular" class='control-label'>Celular</label>
		</div>
		<div class="col-3">
			<input class='form-control' type="text" name='<?php echo $attr['slug'] ?>[celular]' value='<?php echo ($$attr['slug']) ? $$attr['slug']->celular : '' ?>' id='celular'>	
		</div>
	</div><div class="clearfix"></div>


	<div class="row-fluid">
		<div class="col-1 text-right">
			<label for="nextel" class='control-label'>Nextel</label>
		</div>
		<div class="col-3">
			<input class='form-control' type="text" name='<?php echo $attr['slug'] ?>[nextel]' value='<?php echo ($$attr['slug']) ? $$attr['slug']->nextel : '' ?>' id='nextel'>	
		</div>
	</div><div class="clearfix"></div>


	<div class="row-fluid">
		<div class="col-1 text-right">
			<label for="email" class='control-label'>Email</label>
		</div>
		<div class="col-3">
			<input class='form-control' type="text" name='<?php echo $attr['slug'] ?>[email]' value='<?php echo ($$attr['slug']) ? $$attr['slug']->email : '' ?>' id='email'>	
		</div>
	</div><div class="clearfix"></div>



	<div class="row-fluid">
		<div class="col-1 text-right">
			<label for="email_2" class='control-label'>Email 2</label>
		</div>
		<div class="col-3">
			<input class='form-control' type="text" name='<?php echo $attr['slug'] ?>[email_2]' value='<?php echo ($$attr['slug']) ? $$attr['slug']->email_2 : '' ?>' id='email_2'>	
		</div>
	</div><div class="clearfix"></div>






	<div class="row-fluid">
		<div class="col-1 ">
			
		</div>
		<div class="col-2">
			<button type='submit' class='btn btn-sm btn-success'>Salvar</button>
			<button type='button' onClick='history.back()' class='btn btn-sm btn-info'>Voltar</button>
		</div>
	</div><div class="clearfix"></div>



</form>








<script>
	
$("#UploadButton").ajaxUpload({

url : "<?php echo base_url('admin/'.$attr['slug'].'/upload_foto') ?>",
name: "file",
onSubmit: function() {
    $('#InfoBox').html('Uploading ... ');
},
onComplete: function(result) {
	parsed = $.parseJSON(result)
	

    $('#foto_unica').attr('src', parsed['url']).show();
    $('#hidden_foto').val(parsed['filename']);

}
});
</script>










<!---<div class="row-fluid">
	<div class="col-12">
		<legend>Inserir Notícia</legend>
	</div>
</div>
<form class='form-horizontal' method='post' action="<?php echo base_url('admin/setors/inserir') ?>">
<input type="hidden" value='<?php echo date('Y-m-d') ?>' name='setor[data_publicacao]'>
<div class="row-fluid">
	<div class="col-12">
		<div class="col-1 text-right"><label for="titulo" class='control-label'>Título</label></div>
		<div class="col-5"><input type="text" class='form-control' name='setor[titulo]'></div>
	</div>
</div>
<div class="clearfix"></div>



<div class="row-fluid">
	<div class="col-12">
		<div class="col-1 text-right"><label for="excerpt" class='control-label'>Chamada</label></div>
		<div class="col-5"><textarea class='form-control' name='setor[excerpt]'></textarea></div>
	</div>
</div>
<div class="clearfix"></div>

<div class="row-fluid">
	<div class="col-12">
		<div class="col-1 text-right"><label for="titulo" class='control-label'>Texto</label></div>
		<div class="col-11"><textarea class='mce_setor form-control'  rows='7' name='setor[texto]'></textarea></div>
	</div>
</div>
<div class="clearfix"></div>

<div class="row-fluid">
	<div class="col-12">
		<div class="col-1 text-right"><label for="titulo" class='control-label'>Foto</label></div>
		<div class="col-11">	<a id='UploadButton'><button class='btn btn-sm btn-info'>Selecionar Foto</button></a><br><br>	<div class="container_foto_unica">
					<img src="" id='foto_unica'  alt=""><input type="hidden" id='hidden_foto' name='setor[imagem]'> </div>
		</div>
	</div>
</div>
<div class="clearfix"></div>

<div class="row-fluid">
	<div class="col-12">
		<div class="col-1 "></div>
		<div class="col-1"><button class='btn btn-sm btn-success' type='submit'>Salvar</button></div>
		<div class="col-1"><button onClick='history.back(-1)' type='button' class='btn btn-sm btn-info'>Voltar</button></div>
	</div>
</div>
<div class="clearfix"></div>
<div class="row-fluid">
	<div class="col-12">
		<div class="col-1 ">&nbsp;</div>
	</div>
</div>
<div class="clearfix"></div>


</form>

<script>
	
$("#UploadButton").ajaxUpload({

url : "<?php echo base_url('admin/setors/upload_foto') ?>",
name: "file",
onSubmit: function() {
    $('#InfoBox').html('Uploading ... ');
},
onComplete: function(result) {
	parsed = $.parseJSON(result)
	

    $('#foto_unica').attr('src', parsed['url']).show();
    $('#hidden_foto').val(parsed['filename']);

}
});
</script>-->