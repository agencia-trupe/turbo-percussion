<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start(); //we need to call PHP's session object to access it through CI
class Plano_Carreira extends CI_Controller {

    function __construct(){
        parent::__construct();
        $this->objeto       = 'Plano de Carreira';
        $this->objetos      = 'Plano de Carreira';
        $this->slug         = 'plano_carreira';
        $this->genero       = 'o';
        $this->data['attr'] = array(
            'objeto' => $this->objeto,
            'objetos' => $this->objetos,
            'slug' => $this->slug,
            'genero' => $this->genero

        );
    }

    function index()  {
        if ($this->session->userdata('logged_in'))    {

            if(isSet($_POST) && $_POST){
                 $this->db->update($this->slug, $this->input->post($this->slug), array('id' => 1));
                 $this->session->set_flashdata('message', $this->objeto.' editad'.$this->genero .' com sucesso!');
                 redirect('admin/plano_carreira', 'refresh');
            }
                $session_data = $this->session->userdata('logged_in');
                $this->data['title'] = $this->objeto;
                $this->data[$this->slug] = $this->db->get($this->slug)->row();

                $this->load->view('admin/template', $this->data);
        } else {
        redirect('admin/login', 'refresh');
        }
    }





}
