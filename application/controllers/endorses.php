
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start(); //we need to call PHP's session object to access it through CI
class Endorses extends CI_Controller {
  
  public $data;

 function __construct()
 {
  
   parent::__construct();

   $this->data['page'] = 'endorses';
   $this->data['parceiros'] = $this->db->get('parceiros')->result_array();
   $this->data['endorses'] = $this->db->get('endorses')->result();
      $this->data['contato'] = $this->db->get('contato_principal')->row_array();
   
 }

 function index($slug = false)
 {


 	 $divisor = count($this->data['endorses']) / 2;
 	 $this->data['endorses_1'] = array();
 	 $this->data['endorses_2'] = array();
 	 foreach($this->data['endorses'] as $k => $v){
	 	if ($k < $divisor) {	
	 		$this->data['endorses_1'][] = $v;
		}
		else{
			$this->data['endorses_2'][] = $v;
		}
	}
     $this->data['title'] = 'Endorses';
     $this->load->view('layout/template', $this->data);

   }
   
 }



?>

