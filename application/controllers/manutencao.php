<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
class Manutencao extends CI_Controller {  
		
	public $data;

	function __construct(){

		parent::__construct();

		$this->data['page'] = 'manutencao';
		$this->data['contato'] = $this->db->get('contato_principal')->row_array();
		$this->data['parceiros'] = $this->db->get('parceiros')->result_array();
		//$this->data['manutencao'] = $this->db->get('manutencao')->row();
	}

	function index($slug = false){
 		$this->data['title'] = 'Manutenção';
 		$this->load->view('layout/template', $this->data);
	}

	function gerarProtocolo(){

		$nome = $this->input->post('nome');
		$email = $this->input->post('email');
		$nota = $this->input->post('nota');
		$data = $this->input->post('data');
		$loja = $this->input->post('loja');
		$cnpj = $this->input->post('cnpj');
		$endereco = $this->input->post('endereco');
		$telefone = $this->input->post('telefone');
		$bairro = $this->input->post('bairro');
		$cep = $this->input->post('cep');
		$produto_cod = $this->input->post('produto_cod');
		$produto_qtd = $this->input->post('produto_qtd');
		$produto_desc = $this->input->post('produto_desc');
		$produtos = '';

		for ($i=0; $i < sizeof($produto_cod); $i++) { 
			$contador = $i+1;
			$produtos .= <<<STR
------------------------------------<br>
<span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>Produto {$contador}:</span><br>
<span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>Código:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{$produto_cod[$i]}</span><br>
<span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>Quantidade:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{$produto_qtd[$i]}</span><br>
<span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>Problema:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{$produto_desc[$i]}</span><br>
STR;
		}

        if($nome && $email){

			$numero_protocolo = $this->armazenarProtocolo($produtos);

            $emailconf['charset'] = 'utf-8';
            $emailconf['mailtype'] = 'html';
            $emailconf['protocol'] = 'smtp';
            $emailconf['smtp_host'] = 'smtp.turbopercussion.com.br';
            $emailconf['smtp_user'] = 'noreply@turbopercussion.com.br';
            $emailconf['smtp_pass'] = 'noreply156';
            $emailconf['smtp_port'] = 587;

            $this->load->library('email');

            $this->email->initialize($emailconf);

            $from = 'noreply@turbopercussion.com.br';
            $fromname = 'Turbo Percussion';
            $to = 'manutencao@turbopercussion.com.br';
            $cc = $email;
            $assunto = 'Contato via Site';

            $corpoemail = <<<EML
<!DOCTYPE html>
<html>
<head>
    <title>Mensagem de contato via formulário do site</title>
    <meta charset="utf-8">
</head>
<body>
	<span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>Número do Protocolo:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>$numero_protocolo</span><br>
	<span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>Nome:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>$nome</span><br>
	<span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>Email:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>$email</span><br>
	<span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>Número da Nota:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>$nota</span><br>
	<span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>Data da Compra:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>$data</span><br>
	<span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>Nome da Loja:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>$loja</span><br>
	<span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>CNPJ:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>$cnpj</span><br>
	<span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>Endereço:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>$endereco</span><br>
	<span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>Telefone:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>$telefone</span><br>
	<span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>Bairro:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>$bairro</span><br>
	<span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>CEP:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>$cep</span><br><br>
EML;
			$corpoemail .= $produtos;
			$corpoemail .= "</body></html>";

			$plain_produtos = str_replace('<br>', '\r\n', $produtos);
			$plain_produtos = strip_tags($plain_produtos);

            $plain = <<<EML
Número do Protocolo: $numero_protocolo\r\n
Nome: $nome\r\n
Email: $email\r\n
Número da Nota: $nota\r\n
Data da Compra: $data\r\n
Nome da Loja: $loja\r\n
CNPJ: $cnpj\r\n
Endereço: $endereco\r\n
Telefone: $telefone\r\n
Bairro: $bairro\r\n
CEP: $cep\r\n
$plain_produtos
EML;

            $this->email->from($from, $fromname);
            $this->email->to($to);
            $this->email->cc($cc);
            $this->email->bcc('bruno@trupe.net');
            $this->email->reply_to($email);

            $this->email->subject($assunto);
            $this->email->message($corpoemail);
            $this->email->set_alt_message($plain);

            $this->email->send();
        }

		$this->session->set_flashdata('envioprotocolo');
		redirect('manutencao/index', 'refresh');
	}

	private function armazenarProtocolo($produtos){
		$this->db->set('nome', $this->input->post('nome'))
				 ->set('email', $this->input->post('email'))
				 ->set('nota', $this->input->post('nota'))
				 ->set('data', $this->input->post('data'))
				 ->set('loja', $this->input->post('loja'))
				 ->set('cnpj', $this->input->post('cnpj'))
				 ->set('endereco', $this->input->post('endereco'))
				 ->set('telefone', $this->input->post('telefone'))
				 ->set('bairro', $this->input->post('bairro'))
				 ->set('cep', $this->input->post('cep'))
				 ->set('produtos', $produtos)
				 ->insert('protocolos_manutencao');

		return str_pad($this->db->insert_id(), 8, '0', STR_PAD_LEFT);
	}
}