<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start(); //we need to call PHP's session object to access it through CI
setlocale(LC_ALL, "pt_BR", "pt_BR.iso-8859-1", "pt_BR.utf-8", "portuguese");
  date_default_timezone_set('America/Sao_Paulo');
class Videos extends CI_Controller {
  
  public $data;

 function __construct()
 {

   parent::__construct();
   $this->data['page'] = 'videos';
   $this->data['contato'] = $this->db->get('contato_principal')->row_array();
      $this->data['parceiros'] = $this->db->get('parceiros')->result_array();
   $this->data['videos'] = $this->db->get('videos', 4)->result_array();
 }

 function index($slug = false)
 {
  
  if($slug) {
    $this->data['video_destaque'] = $this->db->get_where('videos', array('slug' => $slug))->row_array();
  }
  else{
    $this->data['video_destaque'] = $this->db->get('videos')->row_array();
  }
    $this->data['title'] = 'Vídeos';
    $this->load->view('layout/template', $this->data);

  }


    
 }
 





?>

