<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start(); //we need to call PHP's session object to access it through CI
class onde_comprar extends CI_Controller {
  
  public $data;

 function __construct()
 {
  
   parent::__construct();

   $this->data['page'] = 'onde_comprar';
   $this->data['contato'] = $this->db->get('contato_principal')->row_array();

   $this->data['parceiros'] = $this->db->get('parceiros')->result_array();

   
 }

 function index($slug = false)
 {

 	 
     $this->data['title'] = 'Onde Comprar';
     $this->load->view('layout/template', $this->data);
 }


}



