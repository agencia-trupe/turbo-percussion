<div class="row-fluid">
	<div class="col-lg-12"><?php if ($this->session->flashdata('message')): ?>
		<div class="alert alert-success"> <?php echo $this->session->flashdata('message') ?> </div>
		
	<?php endif ?>
	</div>
</div>

<div class="row-fluid">
	<legend><?php echo $title ?></legend>
</div>
<div class="clearfix"></div>

<form action="<?php echo base_url('admin/'.$attr['slug']) ?>" method='post'>


	<div class="row-fluid">
		<div class="col-1 text-right">
			<label for="telefone" class='control-label'>Telefone</label>
		</div>
		<div class="col-3">
			<input class='form-control ' type="text" name='<?php echo $attr['slug'] ?>[telefone]' id='telefone' value='<?php echo $obj->telefone ?>'>
		</div>
	</div><div class="clearfix"></div>


	<div class="row-fluid">
		<div class="col-1 text-right">
			<label for="fax" class='control-label'>Fax</label>
		</div>
		<div class="col-3">
			<input class='form-control ' type="text" name='<?php echo $attr['slug'] ?>[fax]' id='fax' value='<?php echo $obj->fax ?>'>
		</div>
	</div><div class="clearfix"></div>


	<div class="row-fluid">
		<div class="col-1 text-right">
			<label for="endereco" class='control-label'>Endereço</label>
		</div>
		<div class="col-3">
			<input class='form-control ' type="text" name='<?php echo $attr['slug'] ?>[endereco]' id='endereco' value='<?php echo $obj->endereco ?>'>
		</div>
	</div><div class="clearfix"></div>


	<div class="row-fluid">
		<div class="col-1 text-right">
			<label for="bairro" class='control-label'>Bairro</label>
		</div>
		<div class="col-3">
			<input class='form-control ' type="text" name='<?php echo $attr['slug'] ?>[bairro]' id='bairro' value='<?php echo $obj->bairro ?>'>
		</div>
	</div><div class="clearfix"></div>


	<div class="row-fluid">
		<div class="col-1 text-right">
			<label for="cidade" class='control-label'>Cidade</label>
		</div>
		<div class="col-3">
			<input class='form-control ' type="text" name='<?php echo $attr['slug'] ?>[cidade]' id='cidade' value='<?php echo $obj->cidade ?>'>
		</div>
	</div><div class="clearfix"></div>

	<div class="row-fluid">
		<div class="col-1 text-right">
			<label for="uf" class='control-label'>UF</label>
		</div>
		<div class="col-1">
			<input class='form-control ' type="text" name='<?php echo $attr['slug'] ?>[uf]' id='uf' value='<?php echo $obj->uf ?>'>
		</div>
	</div><div class="clearfix"></div>

	<div class="row-fluid">
		<div class="col-1 text-right">
			<label for="cep" class='control-label'>CEP</label>
		</div>
		<div class="col-3">
			<input class='form-control ' type="text" name='<?php echo $attr['slug'] ?>[cep]' id='cep' value='<?php echo $obj->cep ?>'>
		</div>
	</div><div class="clearfix"></div>

	<div class="row-fluid">
		<div class="col-1 text-right">
			<label for="email_comercial" class='control-label'>Email Comercial</label>
		</div>
		<div class="col-3">
			<input class='form-control ' type="text" name='<?php echo $attr['slug'] ?>[email_comercial]' id='email_comercial' value='<?php echo $obj->email_comercial ?>'>
		</div>
	</div><div class="clearfix"></div>

	<div class="row-fluid">
		<div class="col-1 text-right">
			<label for="facebook" class='control-label'>Facebook</label>
		</div>
		<div class="col-3">
			<input class='form-control ' type="text" name='<?php echo $attr['slug'] ?>[facebook]' id='facebook' value='<?php echo $obj->facebook ?>'>
		</div>
	</div><div class="clearfix"></div>

	<div class="row-fluid">
		<div class="col-1 text-right">
			<label for="twitter" class='control-label'>Twitter</label>
		</div>
		<div class="col-3">
			<input class='form-control ' type="text" name='<?php echo $attr['slug'] ?>[twitter]' id='twitter' value='<?php echo $obj->cidade ?>'>
		</div>
	</div><div class="clearfix"></div>

	<div class="row-fluid">
		<div class="col-1 text-right">
			<label for="youtube" class='control-label'>Youtube</label>
		</div>
		<div class="col-3">
			<input class='form-control ' type="text" name='<?php echo $attr['slug'] ?>[youtube]' id='youtube' value='<?php echo $obj->youtube ?>'>
		</div>
	</div><div class="clearfix"></div>



	<div class="row-fluid">
		<div class="col-1 ">
			
		</div>
		<div class="col-2">
			<button type='submit' class='btn btn-sm btn-success'>Salvar</button>
			<button type='button' onClick='history.back()' class='btn btn-sm btn-info'>Voltar</button>
		</div>
	</div><div class="clearfix"></div>



</form>

<script>
	
$("#UploadButton").ajaxUpload({

url : "<?php echo base_url('admin/'.$attr['slug'].'/upload_foto') ?>",
name: "file",
onSubmit: function() {
    $('#InfoBox').html('Uploading ... ');
},
onComplete: function(result) {
	parsed = $.parseJSON(result)
	

    $('#foto_unica').attr('src', parsed['url']).show();
    $('#hidden_foto').val(parsed['filename']);

}
});
</script>


