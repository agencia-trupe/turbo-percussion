<div class="row-fluid">
	<div class="col-lg-12"><?php if ($this->session->flashdata('message')): ?>
		<div class="alert alert-success"> <?php echo $this->session->flashdata('message') ?> </div>
		
	<?php endif ?>
	</div>
</div>

<div class="row-fluid">
	<legend><?php echo $title ?></legend>
</div>
<div class="clearfix"></div>


<form action="<?php echo base_url('admin/'.$attr['slug']) ?>" method='post'>
	<div class="row-fluid">
		<div class="col-1 text-right">
			<label for="texto_header" class='control-label'>Texto Header</label>
		</div>
		<div class="col-10">
			<textarea class='form-control mce' type="text" name='<?php echo $attr['slug'] ?>[texto_header]'  id='texto_header'>	<?php echo $$attr['slug']->texto_header ?></textarea>
		</div>
	</div><div class="clearfix"></div>

<div class="row-fluid">
		<div class="col-1 text-right">
			<label for="texto_descricao" class='control-label'>Texto Descrição</label>
		</div>
		<div class="col-10">
			<textarea class='form-control mce' type="text" name='<?php echo $attr['slug'] ?>[texto_descricao]'  id='texto_descricao'>	<?php echo $$attr['slug']->texto_descricao ?></textarea>
		</div>
	</div><div class="clearfix"></div>
<div class="row-fluid">
		<div class="col-1 text-right">
			<label for="texto_imagem" class='control-label'>Texto Imagem</label>
		</div>
		<div class="col-10">
			<textarea class='form-control mce' type="text" name='<?php echo $attr['slug'] ?>[texto_imagem]'  id='texto_imagem'>	<?php echo $$attr['slug']->texto_imagem ?></textarea>
		</div>
	</div><div class="clearfix"></div>


	<div class="row-fluid">
		<div class="col-1 ">
			
		</div>
		<div class="col-2">
			<button type='submit' class='btn btn-sm btn-success'>Salvar</button>
			<button type='button' onClick='history.back()' class='btn btn-sm btn-info'>Voltar</button>
		</div>
	</div><div class="clearfix"></div>



</form>