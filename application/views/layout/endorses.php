<div class="banner banner-novidade">
	<div class='wrapper-banner'>Endorses</div>
</div>

<div class="container-corpo">
	<div class="wrapper-corpo">
		<div class="titulo-endorses">Conheça os músicos que recomendam os produtos da Turbo Percussion</div>

		<div class="coluna-endorses-1">
			<?php foreach ($endorses_1 as $key => $v): ?>
				<div class="linha-endorses-2"><img src='<?php echo base_url('assets/img/uploads/endorses/'.$v->url_imagem) ?>' alt='' ><div class='nome-endorse'><?php echo $v->nome ?></div><div class="descricao-endorse"><?php echo $v->descricao ?></div>
				<div class="borda-novidade"></div></div>
			<?php endforeach ?></div>
		<div class="coluna-endorses-2">
			<?php foreach ($endorses_2 as $key => $v): ?>
				
				<div class="linha-endorses-2"><img src='<?php echo base_url('assets/img/uploads/endorses/'.$v->url_imagem) ?>' alt='' ><div class='nome-endorse'><?php echo $v->nome ?></div><div class="descricao-endorse"><?php echo $v->descricao ?></div>
				<div class="borda-novidade"></div></div>
			<?php endforeach ?></div>
		</div>
	</div>
	
</div>

<script>
	jQuery(document).ready(function($) {
		$('.cycle-box').cycle();
	});


</script>
