<div class="banner banner-novidade"  id='banner'>
	<div class='wrapper-banner'>Novidades</div>
</div>

<div class="container-corpo">
	<div class="wrapper-corpo">
		<div class="coluna-novidade">
			<div class="foto-novidade " id='!'>
				<img src='<?php echo base_url('assets/img/uploads/novidades/'.$novidade_destaque['imagem']) ?>' alt='' >
			</div>
			<div class="titulo-novidade titulo-novidade-2"><?php echo $novidade_destaque['titulo'] ?></div>
			<div class="texto-novidade"><?php echo $novidade_destaque['texto'] ?></div>
			<div class="voltar"><a href="<?php echo base_url('novidades') ?>">voltar</a></div>
			<div class="borda-novidade-lateral"></div>
		</div>
				
		<div class="coluna-novidade-2">
			<?php foreach ($novidades as $k => $novidade): ?>
				<a href="<?php echo base_url('novidades/'.$novidade['slug'].'#banner') ?> "><img class='thumb-novidade-2 <?php if ($k == 3): ?> thumb-sem-margem <?php endif ?>' src='<?php echo base_url('assets/img/uploads/novidades/'.$novidade['imagem'].'_thumb.jpg') ?>' alt='' ></a>
				
			<?php endforeach ?>
		</div>
		

	</div>
</div>
