<div class="row-fluid">
	<div class="col-lg-12"><?php if ($this->session->flashdata('message')): ?>
		<div class="alert alert-success"> <?php echo $this->session->flashdata('message') ?> </div>
		
	<?php endif ?>
	</div>
</div>

<div class="row-fluid">
	<legend>Inserir <?php echo $title ?></legend>
</div>
<div class="clearfix"></div>

<form action="<?php echo base_url('admin/'.$attr['slug'].'/'.$this->uri->segment(3)) ?>" method='post'>
<input type="hidden" value='<?php echo date('Y-m-d') ?>' name='<?php echo $attr['slug'] ?>[data_publicacao]'>
	<div class="row-fluid">
		<div class="col-1 text-right">
			<label for="titulo" class='control-label'>Título</label>
		</div>
		<div class="col-3">
			<input class='form-control' type="text" name='<?php echo $attr['slug'] ?>[titulo]' value='<?php echo ($obj) ? $obj->titulo : '' ?>' id='titulo'>	
		</div>
	</div><div class="clearfix"></div>

	<div class="row-fluid">
		<div class="col-1 text-right">
			<label for="excerpt" class='control-label'>Chamada</label>
		</div>
		<div class="col-3">
			<textarea class='form-control ' type="text" name='<?php echo $attr['slug'] ?>[excerpt]' value="" id='excerpt'><?php echo ($obj) ? $obj->excerpt : '' ?></textarea>	
		</div>
	</div><div class="clearfix"></div>

	<div class="row-fluid">
		<div class="col-1 text-right">
			<label for="texto" class='control-label'>Texto</label>
		</div>
		<div class="col-3">
			<input class='form-control' type="text" name='<?php echo $attr['slug'] ?>[texto]' value='<?php echo ($obj) ? $obj->texto : '' ?>' id='texto'>	
		</div>
	</div><div class="clearfix"></div>



	<div class="row-fluid">
		<div class="col-1 text-right">
			<label for="link" class='control-label'>Adicionar Imagem</label>
		</div>
		<div class="col-3">
			<a id='UploadButton'><button class='btn btn-sm btn-info'>Selecionar Foto</button></a><br><br>	<div class="container_foto_unica">
					<img src="" id='foto_unica'  alt=""><input type="hidden" id='hidden_foto'  name='<?php echo $attr['slug'] ?>[imagem]'  value='<?php echo ($obj) ? $obj->imagem: '' ?>'> </div>
			
		</div>
	</div><div class="clearfix"></div>


	<div class="row-fluid">
		<div class="col-1 ">
			
		</div>
		<div class="col-2">
			<button type='submit' class='btn btn-sm btn-success'>Salvar</button>
			<button type='button' onClick='history.back()' class='btn btn-sm btn-info'>Voltar</button>
		</div>
	</div><div class="clearfix"></div>



</form>








<script>
	
$("#UploadButton").ajaxUpload({

url : "<?php echo base_url('admin/'.$attr['slug'].'/upload_foto') ?>",
name: "file",
onSubmit: function() {
    $('#InfoBox').html('Uploading ... ');
},
onComplete: function(result) {
	parsed = $.parseJSON(result)
	$('#foto_unica').attr('src', parsed['url']).show();
    $('#hidden_foto').val(parsed['filename']);

}
});
</script>


