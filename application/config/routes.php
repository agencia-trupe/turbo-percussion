<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';''
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "home";
$route['business_practices/(:any)'] = "business_practices/index/$1";

$route['novidades/(:any)'] = "novidades/index/$1";
$route['videos/(:any)'] = "videos/index/$1";
$route['produtos/download/(:any)'] = "produtos/download/$1";
$route['produtos/visualizar/(:any)'] = "produtos/visualizar/$1";
$route['produtos/pesquisar/(:any)'] = "produtos/pesquisar/$1";
$route['produtos/subcategoria/(:any)'] = "produtos/subcategoria/$1";
$route['produtos/(:any)'] = "produtos/index/$1";
$route['404_override'] = '';
$route['admin'] = 'admin/login';

/* End of file routes.php */
/* Location: ./application/config/routes.php */