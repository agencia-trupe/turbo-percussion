<?php

$lang['required']			= "Preencha todos os campos do formul&aacute;rio";
$lang['isset']				= "O campo %s deve have a value.";
$lang['valid_email']		= "O campo %s deve conter a valid email address.";
$lang['valid_emails']		= "O campo %s deve conter all valid email addresses.";
$lang['valid_url']			= "O campo %s deve conter a valid URL.";
$lang['valid_ip']			= "O campo %s deve conter a valid IP.";
$lang['min_length']			= "O campo %s deve be at least %s characters in length.";
$lang['max_length']			= "O campo %s não pode exceed %s characters in length.";
$lang['exact_length']		= "O campo %s deve ter o tamanho exato de  %s";
$lang['alpha']				= "O campo %s apenas conter catacteres alfa-numéricos.";
$lang['alpha_numeric']		= "O campo %s apenas conter alpha-numeric characters.";
$lang['alpha_dash']			= "O campo %s apenas conter alpha-numeric characters, underscores, and dashes.";
$lang['numeric']			= "O campo %s deve conter only numbers.";
$lang['is_numeric']			= "O campo %s deve conter only numeric characters.";
$lang['integer']			= "O campo %s deve conter an integer.";
$lang['regex_match']		= "O campo %s is not in the correct format.";
$lang['matches']			= "O campo %s does not match the %s field.";
$lang['is_unique'] 			= "O campo %s deve conter a unique value.";
$lang['is_natural']			= "O campo %s deve conter only positive numbers.";
$lang['is_natural_no_zero']	= "O campo %s deve conter a number greater than zero.";
$lang['decimal']			= "O campo %s deve conter a decimal number.";
$lang['less_than']			= "O campo %s deve conter a number less than %s.";
$lang['greater_than']		= "O campo %s deve conter a number greater than %s.";


/* End of file form_validation_lang.php */
/* Location: ./system/language/english/form_validation_lang.php */