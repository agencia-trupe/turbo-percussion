<div class="row-fluid">
	<div class="col-lg-12"><?php if ($this->session->flashdata('message')): ?>
		<div class="alert alert-success"> <?php echo $this->session->flashdata('message') ?> </div>
		
	<?php endif ?>
	</div>
</div>

<div class="row-fluid">
	<legend><?php echo $attr['objetos'] ?>
		<a href="<?php echo base_url('admin/'.$attr['slug'].'/gerar_excel') ?> ">
			<button class='btn btn-xs btn-info'>
				<span class='glyphicon glyphicon-th-list'>  Gerar Relatório</span>
			</button>
		</a>
		<a href="<?php echo base_url('admin/'.$attr['slug'].'/gerar_excel') ?> ">
			
		</a>
				
	</legend>
</div>
<div class="clearfix"></div>
<div class="row-fluid">
	<div class="col-2"></div>
	<div class="col-8">
		<table class='table table-stripped table-condensed'>
			<thead>
				
				<tr>
					<th>Nome</th><th>Email</th><th>Hora de cadastro</th><th>Ações</th>
				</tr>
			</thead>
			<tbody>
				
				<?php foreach ($$attr['slug'] as $i => $v): ?>
					<tr id='item_<?php echo $v->id ?>	'>
						<td><?php echo $v->nome ?></td><td><?php echo $v->email ?></td><td><?php echo date('d/m/Y H:i:s', strtotime($v->timestamp)) ?></td><td>
						<a onclick="if(confirm('Tem certeza que deseja deletar <?php echo $attr['genero'] ?> <?php echo $attr['objeto'] ?>? Essa operação é irreversível.')) window.location = '<?php echo base_url('admin/'.$attr['slug'].'/deletar/'.$v->id ) ?>'"><button title='Deletar <?php echo $attr['objeto'] ?>' class='btn btn-xs btn-info'><span class="glyphicon glyphicon-remove"></span></button></a> 
					</tr>
				<?php endforeach ?>
			</tbody>
		</table>	
	</div>
</div>



 <script>
  
</script>