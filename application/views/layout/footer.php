
<div class="parceiros <?php if ($this->uri->uri_string() == 'home' || $this->uri->uri_string() == ''): ?> parceiros-home<?php endif; ?> "> 
<div class="marcas-parceiras"><span>Marcas Parceiras</span></div>
    <div class="wrapper-parceiros">
        <div class="cyclebox-parceiros">
            <div class="cycle-parceiro">
                
            <?php foreach ($parceiros as $key => $parceiro): ?>
                <?php $aux = ++$key ?>
                <?php if (!($aux % 6)): ?>
                    </div><div class="cycle-parceiro">
                <?php endif ?>
                <img class='foto-parceiros' src='<?php echo base_url('assets/img/uploads/parceiros/'.$parceiro['url_imagem']) ?>' alt='' >

                
            <?php endforeach ?>


            </div>
        </div>
    </div>

</div>
<footer>
    <div class="wrapper-footer">
    	<div class="coluna-footer-1">

    		<div class="titulo-coluna-footer">Telefone</div>
    		<div class="texto-coluna-footer">Tel: <?php echo $contato['telefone'] ?> | FAX: <?php echo $contato['fax'] ?></div>

    	</div>
    	<div class="coluna-footer-2">
    		<div class="titulo-coluna-footer">Endereço</div>
    		<div class="texto-coluna-footer"><?php echo $contato['endereco'] ?> - <?php echo $contato['bairro'] ?> - <?php echo $contato['cidade'] ?> / <?php echo $contato['uf'] ?> - CEP: <?php echo $contato['uf'] ?></div>
    	</div>
    </div>
</footer>
    
    <div class="faixa-azul-footer">
    	<div class="criacao-sites"><a href="http://trupe.net">Criação de Sites:</a> <a href="http://trupe.net">Trupe Agência Criativa</a></div>
    </div>

</div>
    
<script>

    jQuery(document).ready(function($) {
        $('.cycle-box').cycle({
            fx: 'scrollRight'  ,
            speed: 1400,
            timeout: 8000,
            pager:  '.slide-nav',
            activePagerClass: 'activeSlide',
            manualTrump: false
        });
        $('.slide-nav a').html('');
        
        $('.enviar-newsletter').on('click',function(){
            $('#form-newsletter').submit();
        })
        <?php if ($this->session->flashdata('newsletter_sucesso')): ?>
            alert('<?php echo $this->session->flashdata('newsletter_sucesso') ?> ');
    
        <?php endif ?>

    });

    jQuery(document).ready(function($) {

  

        $('.form-pesquisa-produto').on('submit', function(){
            window.location = "<?php echo base_url('produtos/pesquisar') ?>"+'/'+$('#query-pesquisa').val();
            return false;
        })

        $('.icone-pesquisa').on('click', function(){
            $('.form-pesquisa-produto').submit();
        })


    $('.icone-header').hover(function(){
        $(this).attr('src', $(this).attr('data-hover'))
    }, function(){
        $(this).attr('src', $(this).attr('data-url'))
        
    })

    }); 



    $(window).load(function(){
        $('.cyclebox-parceiros').cycle({
            fx: 'scrollRight'   
        });

    })


</script>
</body>
</html>