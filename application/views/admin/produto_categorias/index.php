<div class="row-fluid">
	<div class="col-lg-12"><?php if ($this->session->flashdata('message')): ?>
		<div class="alert alert-success"> <?php echo $this->session->flashdata('message') ?> </div>
		
	<?php endif ?>
	</div>
</div>

<div class="row-fluid">
	<legend><?php echo $attr['objetos'] ?>
 		<a href="<?php echo base_url('admin/'.$attr['slug'].'/inserir') ?> ">
			<button class='btn btn-xs btn-success'>
				<span class='glyphicon glyphicon-plus'> Item</span>
			</button>
		</a>
		<a href="<?php echo base_url('admin/'.$attr['slug'].'/categorias') ?> ">
			
		</a>
				<span class="text-info alterar_ordem_info">Você pode alterar a ordem de exibição simplesmente arrastando as linhas.</span>
	</legend>
</div>
<div class="clearfix"></div>
<div class="row-fluid">
	<div class="col-2"></div>
	<div class="col-8">
		<table class='table table-stripped table-condensed'>
			<thead>
				
				<tr>
					<th>Título</th><th>Ações</th>
				</tr>
			</thead>
			<tbody>
				
				<?php foreach ($obj as $i => $v): ?>
					<tr id='item_<?php echo $v->id ?>	'>
						<td><?php echo $v->titulo ?></td><td>
						<a class='botao_tabela' href="<?php echo base_url('admin/produto_subcategorias/por_categoria/'.$v->id) ?>"><button title='Visualizar Subcategorias' class='btn btn-xs btn-info'><span class="glyphicon glyphicon-eye-open"></span></button></a>
						<a class='botao_tabela' href="<?php echo base_url('admin/'.$attr['slug'].'/editar/'.$v->id) ?>"><button title='Editar <?php echo $attr['objeto']; ?>' class='btn btn-xs btn-info'><span class="glyphicon glyphicon-pencil"></span></button></a>
						<a onclick="if(confirm('Tem certeza que deseja deletar <?php echo $attr['genero'] ?> <?php echo $attr['objeto'] ?>? Essa operação é irreversível.')) window.location = '<?php echo base_url('admin/'.$attr['slug'].'/deletar/'.$v->id ) ?>'"><button title='Deletar <?php echo $attr['objeto'] ?>' class='btn btn-xs btn-info'><span class="glyphicon glyphicon-remove"></span></button></a> 
					</tr>
				<?php endforeach ?>
			</tbody>
		</table>	
	</div>
</div>



 <script>
$("table tbody").sortable({


  		update: function( event, ui ) {
			data = $('table tbody').sortable('serialize')
			
			$.post('<?php echo base_url('admin/'.$attr['slug'].'/grava_ordem') ?>', data)
			
		}
});
  
</script>