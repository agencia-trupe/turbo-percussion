<div class="banner banner-manutencao">
	<div class='wrapper-banner'>Manutenção</div>
</div>

<div class="container-corpo">
	<div class="wrapper-corpo">

		<div class="texto-manutencao-1">Para facilitarmos a manutenção do seu equipamento, preencha o formulário abaixo</div>
		
		<?php if ($this->session->flashdata('envioprotocolo')): ?>			
			<div class="mensagem-envio-manutencao">
				Seu protocolo foi gerado e enviado com sucesso!<br>
				Você receberá uma cópia do protocolo de manutenção no email informado.
			</div>
		<?php endif ?>		

		<form action="<?=base_url('manutencao/gerarProtocolo')?>" method="post" id='form-protocolo'>
			<div class="coluna-manutencao-1">
				<div class="linha-form">
					<label for="nome" class='label-form-1'>Nome</label>
					<input type="text" class='input-form-1' name='nome' id='input-nome' required data-msg="Informe seu nome">
				</div>

				<div class="linha-form">
					<label for="email" class='label-form-1'>Email</label>
					<input type="text" class='input-form-1' name='email' id='input-email' required data-msg="Informe seu e-mail">
				</div>

				<div class="linha-form">
					<label for="nota" class='label-form-1'>Nº da Nota</label>
					<input type="text" class='input-form-1' name='nota' id='input-nota'>
				</div>

				<div class="linha-form">
					<label for="data" class='label-form-1'>Data da Compra</label>
					<input type="text" class='input-form-1' name='data' id='input-data'>
				</div>

				<div class="linha-form">
					<label for="loja" class='label-form-1'>Nome da Loja</label>
					<input type="text" class='input-form-1' name='loja' id='input-loja' required data-msg="Informe o nome da Loja em que comprou o equipamento">
				</div>

				<div class="linha-form">
					<label for="cnpj" class='label-form-1'>CNPJ</label>
					<input type="text" class='input-form-1' name='cnpj' id='input-cnpj'>
				</div>

				<div class="linha-form">
					<label for="endereco" class='label-form-1'>Endereço</label>
					<input type="text" class='input-form-1' name='endereco' id='input-endereco' required data-msg="Informe seu endereço">
				</div>

				<div class="linha-form">
					<label for="telefone" class='label-form-1'>Telefone</label>
					<input type="text" class='input-form-1' name='telefone' id='input-telefone' required data-msg="Informe um telefone de contato">
				</div>

				<div class="linha-form">
					<label for="bairro" class='label-form-1'>Bairro</label>
					<input type="text" class='input-form-1' name='bairro' id='input-bairro'>
				</div>

				<div class="linha-form">
					<label for="cep" class='label-form-1'>CEP</label>
					<input type="text" class='input-form-1' name='cep' id='input-cep'>
				</div>
			</div>

			<div class="coluna-manutencao-1">
				<div id="container-produtos">
					
					<div class="form-produto">

						<div class="linha-form">
							<label class='label-form-1 label-titulo'>Produto <span class='contador-produtos'>1</span></label>						
						</div>

						<div class="linha-form">
							<label for="produto_cod" class='label-form-1'>Cód. do Produto</label>
							<input type="text" class='input-form-1 prod_cod' name='produto_cod[]' required data-msg="Informe o código do produto">
						</div>

						<div class="linha-form">
							<label for="produto_qtd" class='label-form-1'>Quantidade</label>
							<input type="text" class='input-form-1 prod_qtd' name='produto_qtd[]' required data-msg="Informe a quantidade de produtos">
						</div>

						<div class="linha-form">
							<label for="produto_desc" class='label-form-1'>Desc. do Problema</label>
							<textarea type="text" class='textarea-form-1 prod_desc' name='produto_desc[]' required data-msg="Envie uma descrição do problema"></textarea>
						</div>

					</div>

				</div>
				<div class="linha-form">
					<button type='button' class='botao-form botao-incluir'>incluir mais produtos</button>
					<input type='submit' class='botao-form botao-processar' value='gerar protocolo'>
				</div>
			</div>
		</form>
	</div>
	
</div>

<script defer>
	$('document').ready( function(){
		
		var contador = 1;

		$('.botao-incluir').on('click', function(){
			contador++;
			var el = $(this);
			var clone = $('.form-produto:first').clone();
			var botao_remover = $("<a href='#' class='botao-remover' title='Remover este produto'>(remover este produto)</a>");
			clone.find('.label-titulo').append(botao_remover);
			clone.find('.contador-produtos').html(contador);
			clone.find('.prod_cod').val('');
			clone.find('.prod_qtd').val('');
			clone.find('.prod_desc').val('');
			clone.appendTo('#container-produtos');
		});

		$('#form-protocolo').on('click', '.botao-remover', function(e){
			e.preventDefault();
			contador--;
			var contador_local = 1;
			var el = $(this);
			el.parent().parent().parent().remove();
			$('.form-produto').each( function(){
				$(this).find('.contador-produtos').html(contador_local);
				contador_local++;
			});
		});

		$('#form-protocolo').submit( function(e){
			$('[required]').each( function(){
				if($(this).val() == ''){
					alert($(this).attr('data-msg'));
					$(this).focus();
					e.preventDefault();
					return false;
				}
			});
		});

		if($('.mensagem-envio-manutencao').length){
			setTimeout( function(){
				$('.mensagem-envio-manutencao').addClass('removido');
			}, 4000);
		}

	});
</script>