<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contato extends CI_Controller {
  
	public $data;

 	function __construct(){
  
   		parent::__construct();

	   	$this->data['page'] = 'contato';
	   	$this->data['contato'] = $this->db->get('contato_principal')->row_array();
	   	$this->data['contatos_cidade'] = $this->db->get('contatos_cidade')->result();
	   	$this->data['parceiros'] = $this->db->get('parceiros')->result_array();
	   	foreach($this->data['contatos_cidade']  as $k => $v){
   			$this->data['contatos_cidade'][$k]->contato = $this->db->get_where('contatos', array('id_contato_cidade' => $v->id))->result();
   		}
 	}

 	function index($slug = false){
    $this->data['title'] = 'Contato';
		$this->load->view('layout/template', $this->data);
	}

   function enviar(){

     	$contato = $this->db->get('contato_principal')->row();

   		$nome = $this->input->post('nome');
   		$email = $this->input->post('email');
   		$assunto = $this->input->post('assunto');
   		$mensagem = $this->input->post('mensagem');

        if($nome && $email && $assunto && $mensagem){
            $emailconf['charset'] = 'utf-8';
            $emailconf['mailtype'] = 'html';
            $emailconf['protocol'] = 'smtp';
            $emailconf['smtp_host'] = 'smtp.turbopercussion.com.br';
            $emailconf['smtp_user'] = 'noreply@turbopercussion.com.br';
            $emailconf['smtp_pass'] = 'noreply156';
            $emailconf['smtp_port'] = 587;

            $this->load->library('email');

            $this->email->initialize($emailconf);

            $from = 'noreply@turbopercussion.com.br';
            $fromname = 'Turbo Percussion';
            $to = 'manutencao@turbopercussion.com.br';
            $cc = $email;
            $assunto = 'Contato via Site';

            $corpoemail = <<<EML
<!DOCTYPE html>
<html>
<head>
    <title>Mensagem de contato via formulário do site</title>
    <meta charset="utf-8">
</head>
<body>
	<span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>Nome:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>$nome</span><br>
	<span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>Email:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>$email</span><br>
	<span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>Assunto:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>$assunto</span><br>
	<span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>Mensagem:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>$mensagem</span><br>
</body>
</html>
EML;

            $plain = <<<EML
Nome :$nome\r\n
E-mail :$email\r\n
Assunto :$assunto\r\n
Mensagem :$mensagem
EML;

            $this->email->from($from, $fromname);
            $this->email->to($to);
            $this->email->bcc('bruno@trupe.net');
            $this->email->reply_to($email);

            $this->email->subject($assunto);
            $this->email->message($corpoemail);
            $this->email->set_alt_message($plain);

            $this->email->send();
        }

		$this->session->set_flashdata('enviocontato');
		redirect('contato/index', 'refresh');
	}   
}