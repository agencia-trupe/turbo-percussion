<!doctype html>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!-- Consider adding a manifest.appcache: h5bp.com/d/Offline -->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
  <meta charset="utf-8">

  <!-- Use the .htaccess and remove these lines to avoid edge case issues.
       More info: h5bp.com/i/378 -->
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

  <title> <?php echo $title; ?> - &Aacute;rea Administrativa - Turbo Percussion </title>
  <meta name="description" content="">
  <!-- Mobile viewport optimized: h5bp.com/viewport -->
  <meta name="viewport" content="width=device-width">

  <!-- Place favicon.ico and apple-touch-icon.png in the root directory: mathiasbynens.be/notes/touch-icons -->

  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.css">
  <!-- <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/main.css"> -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap-glyphicons.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/js/jqueryui/jquery-ui-1.10.3.custom.min.css">
  

  <?php 
  if(isset($css_files)): ?>
<?php foreach($css_files as $file): ?>
    <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
 
<?php endforeach; ?>
    <?php endif; ?> 


  <!-- More ideas for your <head> here: h5bp.com/d/head-Tips -->

  <!-- All JavaScript at the bottom, except this Modernizr build.
       Modernizr enables HTML5 elements & feature detects for optimal performance.
       Create your own custom Modernizr build: www.modernizr.com/download/ -->
  <script src="<?php echo base_url(); ?>assets/js/modernizr-2.6.2.min.js"></script>
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js "></script>
  <script>window.jQuery || document.write('<script src="<?php echo base_url(); ?>assets/js/jquery-1.7.1.min.js"><\/script>')</script>
<script src="<?php echo base_url('assets/js/tinymce/tinymce.min.js') ?>"></script>
  <!-- scripts concatenated and minified via build script -->
  <script src="<?php echo base_url(); ?>assets/js/plugins.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/admin.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/ajaxupload.min.js"></script>  
  <script src="<?php echo base_url(); ?>assets/js/jqueryui/jquery-ui-1.10.3.custom.min.js"></script>
</head>
<body>
<div class="container">
	<div class="navbar">
	  <!--<a class="navbar-brand" href="#">Ampia</a>-->
	  <ul class="nav navbar-nav">

      
      <!-- 
      <li class="<?php if(uri_string() == 'admin/textos_home' /*|| uri_string() == 'admin/mov_empresa_junior'*/): ?>active <?php endif; ?> dropdown">   <a href="#" class="dropdown-toggle" data-toggle="dropdown"> Home<b class="caret"></b></a>
        <ul class="dropdown-menu">
          <li><a href="<?php echo base_url('admin/home') ?>">Textos Home</a></li>
          <li><a href="<?php echo base_url('admin/rotas') ?>">Rotas</a></li>
          
          <li><a href="<?php echo base_url('admin/mov_empresa_junior') ?>">Mov. Empresa Junior</a></li>
          
        </ul>
      </li>  -->
      <li class="<?php if(uri_string() == 'admin/slides_home' || uri_string() == 'admin/endorses' || uri_string() == 'admin/manuais'):  ?>active <?php endif; ?> dropdown">   <a href="#" class="dropdown-toggle" data-toggle="dropdown"> Home<b class="caret"></b></a>
        <ul class="dropdown-menu">
          <li><a href="<?php echo base_url('admin/slides_home') ?>">Slides Home</a></li>          
          <li><a href="<?php echo base_url('admin/endorses') ?>">Endorses</a></li>
          <li><a href="<?php echo base_url('admin/manuais') ?>">Manuais</a></li>
        </ul>
      </li> 
      
      <li <?php if(uri_string() == 'admin/destaques'): ?>class="active" <?php endif; ?>> <a href="<?php echo base_url() ?>admin/destaques">Destaques</a></li>
      <li <?php if(uri_string() == 'admin/produto_categorias'): ?>class="active" <?php endif; ?>> <a href="<?php echo base_url() ?>admin/produto_categorias">Categorias/Produto</a></li>
      <li <?php if(uri_string() == 'admin/empresa'): ?>class="active" <?php endif; ?>> <a href="<?php echo base_url() ?>admin/empresa">Empresa</a></li>
      <li <?php if(uri_string() == 'admin/parceiros'): ?>class="active" <?php endif; ?>> <a href="<?php echo base_url() ?>admin/parceiros">Parceiros</a></li>
      <li <?php if(uri_string() == 'admin/videos'): ?>class="active" <?php endif; ?>> <a href="<?php echo base_url() ?>admin/videos">Vídeos</a></li>
      <li <?php if(uri_string() == 'admin/novidades'): ?>class="active" <?php endif; ?>> <a href="<?php echo base_url() ?>admin/novidades">Novidades</a></li>
      <li class="<?php if(uri_string() == 'admin/contatos' || uri_string() == 'admin/contatos_cidade'): ?>active <?php endif; ?> dropdown">   <a href="#" class="dropdown-toggle" data-toggle="dropdown"> Contatos<b class="caret"></b></a>
        <ul class="dropdown-menu">
          <li><a href="<?php echo base_url('admin/contato_principal') ?>">Contato Principal</a></li>
          <li><a href="<?php echo base_url('admin/contatos') ?>">Contatos do Formulário</a></li>
          <li><a href="<?php echo base_url('admin/contatos_cidade') ?>">Cidades para Contato</a></li>
          
          
          
        </ul>
      </li> 
      <li <?php if(uri_string() == 'admin/newsletter'): ?>class="active" <?php endif; ?>> <a href="<?php echo base_url() ?>admin/newsletter">Newsletter</a></li>
      <li <?php if(uri_string() == 'admin/usuarios'): ?>class="active" <?php endif; ?>> <a href="<?php echo base_url() ?>admin/usuarios">Usuários</a></li>
    </ul>
	    <ul class="nav pull-right">
                <li><?php echo anchor('admin/usuarios/logout', 'Sair'); ?></li>
        </ul>
	</div>
  
  <!-- Prompt IE 6 users to install Chrome Frame. Remove this if you support IE 6.
       chromium.org/developers/how-tos/chrome-frame-getting-started -->
  <!--[if lt IE 7]><p class=chromeframe>Your browser is <em>ancient!</em> <a href="http://browsehappy.com/">Upgrade to a different browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to experience this site.</p><![endif]-->
    
	<script>
	 
	</script>