<div class="row-fluid">
	<div class="col-12">
		<legend>Inserir Notícia</legend>
	</div>
</div>
<form class='form-horizontal' method='post' action="<?php echo base_url('admin/noticias/inserir') ?>">
<input type="hidden" value='<?php echo date('Y-m-d') ?>' name='noticia[data_publicacao]'>
<div class="row-fluid">
	<div class="col-12">
		<div class="col-1 text-right"><label for="titulo" class='control-label'>Título</label></div>
		<div class="col-5"><input type="text" class='form-control' name='noticia[titulo]'></div>
	</div>
</div>
<div class="clearfix"></div>



<div class="row-fluid">
	<div class="col-12">
		<div class="col-1 text-right"><label for="excerpt" class='control-label'>Chamada</label></div>
		<div class="col-5"><textarea class='form-control' name='noticia[excerpt]'></textarea></div>
	</div>
</div>
<div class="clearfix"></div>

<div class="row-fluid">
	<div class="col-12">
		<div class="col-1 text-right"><label for="titulo" class='control-label'>Texto</label></div>
		<div class="col-11"><textarea class='mce_noticia form-control'  rows='7' name='noticia[texto]'></textarea></div>
	</div>
</div>
<div class="clearfix"></div>

<div class="row-fluid">
	<div class="col-12">
		<div class="col-1 text-right"><label for="titulo" class='control-label'>Foto</label></div>
		<div class="col-11">	<a id='UploadButton'><button class='btn btn-sm btn-info'>Selecionar Foto</button></a><br><br>	<div class="container_foto_unica">
					<img src="" id='foto_unica'  alt=""><input type="hidden" id='hidden_foto' name='noticia[imagem]'> </div>
		</div>
	</div>
</div>
<div class="clearfix"></div>

<div class="row-fluid">
	<div class="col-12">
		<div class="col-1 "></div>
		<div class="col-1"><button class='btn btn-sm btn-success' type='submit'>Salvar</button></div>
		<div class="col-1"><button onClick='history.back(-1)' type='button' class='btn btn-sm btn-info'>Voltar</button></div>
	</div>
</div>
<div class="clearfix"></div>
<div class="row-fluid">
	<div class="col-12">
		<div class="col-1 ">&nbsp;</div>
	</div>
</div>
<div class="clearfix"></div>


</form>

<script>
	
$("#UploadButton").ajaxUpload({

url : "<?php echo base_url('admin/noticias/upload_foto') ?>",
name: "file",
onSubmit: function() {
    $('#InfoBox').html('Uploading ... ');
},
onComplete: function(result) {
	parsed = $.parseJSON(result)
	

    $('#foto_unica').attr('src', parsed['url']).show();
    $('#hidden_foto').val(parsed['filename']);

}
});
</script>