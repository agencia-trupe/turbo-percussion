<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start(); //we need to call PHP's session object to access it through CI
class produtos extends CI_Controller {

    function __construct(){
        parent::__construct();
        $this->objeto       = 'Produto';
        $this->objetos      = 'Produtos';
        $this->slug         = 'produtos';
        $this->genero       = 'o';
        $this->data['attr'] = array(
            'objeto' => $this->objeto,
            'objetos' => $this->objetos,
            'slug' => $this->slug,
            'genero' => $this->genero

        );
    }

    function por_subcategoria($id) {
        if ($this->session->userdata('logged_in'))    {
          if(!$id){
            $id = $this->session->userdata('subcategoria_selecionada');
          }
          $this->session->set_userdata('subcategoria_selecionada', $id);
            $session_data = $this->session->userdata('logged_in');
            $this->data['title'] = $this->objeto;
            $this->data['obj'] = $this->db->order_by('ordem')->get_where($this->slug, array('id_subcategoria'=> $id))->result();
            $this->load->view('admin/template', $this->data);
        } else {
        redirect('admin/login', 'refresh');
        }
    }

    function editar($id)  {
        if ($this->session->userdata('logged_in'))    {

            if(isSet($_POST) && $_POST){
                
                $this->load->model('Produto');
                $obj = $this->input->post();
                $obj['produtos']['id'] = $id;

                $obj['produtos']['slug'] =  $this->toAscii($obj['produtos']['nome']);
                if (isSEt($_FILES['userfile']['name']) && !empty($_FILES['userfile']['name'])) {
                  $this->upload_manual();
                  $obj['produtos']['url_manual'] = $_FILES['userfile']['name'];
                }


                $this->Produto->edita_produto($obj);


                $this->session->set_flashdata('message', $this->objeto.' editad'.$this->genero .' com sucesso!');
                redirect('admin/'.$this->slug.'/por_subcategoria/'.$this->session->userdata('subcategoria_selecionada'), 'refresh');
            }
                $session_data = $this->session->userdata('logged_in');
                $this->data['title'] = $this->objeto;
                $this->data['obj'] = $this->db->get_where($this->slug, array('id' => $id))->row();
                $this->data['produto_caracteristicas'] = $this->db->get_where('produto_caracteristicas', array('id_produto' => $id))->result();
                
                $this->data['produto_fotos'] = $this->db->order_by('ordem')->get_where('produto_fotos', array('id_produto' => $id))->result();
                

                $this->load->view('admin/template', $this->data);
        } else {
        redirect('admin/login', 'refresh');
        }
    }

private function toAscii($str, $replace=array(), $delimiter='-') {
      if( !empty($replace) ) {
        $str = str_replace((array)$replace, ' ', $str);
      }

      $clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
      $clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
      $clean = strtolower(trim($clean, '-'));
      $clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);

      return $clean;
    }


    function inserir()  {
        if ($this->session->userdata('logged_in'))    {

              if(isSet($_POST) && $_POST){
                
                $this->load->model('Produto');

                $obj = $this->input->post();
                $obj['produtos']['slug'] =  $this->toAscii($obj['produtos']['nome']);
                if (isSEt($_FILES['userfile']['name'] ) && !empty($_FILES['userfile']['name'])) {
                  $this->upload_manual();
                  $obj['produtos']['url_manual'] = $_FILES['userfile']['name'];
                }
                
                $this->Produto->insere_produto($obj);
                  
                

                $this->session->set_flashdata('message', $this->objeto.' inserid'.$this->genero .' com sucesso!');
                
                redirect('admin/'.$this->slug.'/por_subcategoria/'.$this->session->userdata('subcategoria_selecionada'), 'refresh');
            }
                $session_data = $this->session->userdata('logged_in');
                $this->data['title'] = $this->objeto;
                $this->data['obj'] = '';

                $this->load->view('admin/template', $this->data);
        } else {
        redirect('admin/login', 'refresh');
        }
    }


    private function upload_manual(){

    $config['upload_path'] = './assets/img/uploads/produto_manuais';
    #$config['allowed_types'] = 'txt|csv';
    $config['max_size'] = '0';
    $config['max_width']  = '0';

    $config['allowed_types'] = '*';


    $config['max_height']  = '0';
    $this->load->library('upload', $config);
    
    if (!$this->upload->do_upload()) {
        die($this->upload->display_errors());
        
    } else 
        return true;


    }
  function deletar($id){

    if($this->session->userdata('logged_in'))  {

      $session_data = $this->session->userdata('logged_in');


      $this->db->delete($this->slug, array('id' => $id));

      $this->session->set_flashdata('message', $this->objeto." deletad".$this->genero." com sucesso!");
      redirect('admin/'.$this->slug.'/por_subcategoria/'.$this->session->userdata('subcategoria_selecionada'), 'refresh');

    } else {
    //If no session, redirect to login page
    redirect('admin/login', 'refresh');
    }
  }


  function grava_ordem(){
    if($this->session->userdata('logged_in')) {
      $post = $this->input->post();
      $textos_banner = $this->db->get($this->slug)->result();

      foreach ($post['item'] as $k => $v) {
      $this->db->update($this->slug, array('ordem' => ++$k), array('id' => $v));
      }

    }  else  {
    //If no session, redirect to login page
    redirect('admin/login', 'refresh');
    }

  }




   function upload_foto($id = ''){
      
      $config['upload_path'] = "./assets/img/uploads/produto_fotos";
      $config['allowed_types'] = 'gif|jpg|png|doc|txt';
      $config['max_size']  = 0;
      $config['encrypt_name'] = TRUE;

      $file_element_name = 'file';
      $this->load->library('upload', $config);
  
      if (!$this->upload->do_upload($file_element_name))
      {
         $status = 'error';
         $msg = $this->upload->display_errors('', '').$config['upload_path'];
         $data['file_name'] = '';
      }
      else
      {
         $data = $this->upload->data();
      
         if($data['file_name'])
         {
            $status = "success";
            $msg = "File successfully uploaded";
         }
         else
         {
            unlink($data['full_path']);
            $status = "error";
            $msg = "Something went wrong when saving the file, please try again.";
         }
         $this->load->library('image_moo');
         $this->image_moo->load($data['full_path'])->set_jpeg_quality(100)->resize_crop(71,71)->save($data['full_path'].'_thumb.jpg');
         $this->image_moo->load($data['full_path'])->set_jpeg_quality(100)->resize_crop(240,200)->save($data['full_path'].'_lista.jpg');
         $this->image_moo->load($data['full_path'])->set_jpeg_quality(100)->resize_crop(220,90)->save($data['full_path'].'_destaque.jpg');
         $this->image_moo->load($data['full_path'])->set_jpeg_quality(100)->resize(525,9999)->save($data['full_path'], TRUE);
         if ($this->image_moo->errors) print $this->image_moo->display_errors();
      }

      @unlink($_FILES[$file_element_name]);
         echo json_encode(array('status' => $status, 'msg' => $msg, 'url' => base_url().'assets/img/uploads/produto_fotos/'.$data['file_name'], 'filename' => $data['file_name']));
}



function grava_ordem_fotos_produto($id_produto){
   if($this->session->userdata('logged_in'))
   {
    $post = $this->input->post();
    $categorias = $this->db->order_by('ordem')->get_where('produto_fotos', array('id_produto' => $id_produto))->result();

    foreach ($post['item'] as $k => $v) {
        $this->db->update('produto_fotos', array('ordem' => ++$k), array('id' => $v));
    }

   }
   else
   {
     //If no session, redirect to login page
     redirect('admin/login', 'refresh');
   }

}



}
