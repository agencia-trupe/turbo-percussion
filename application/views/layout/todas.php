<div class="banner banner-novidade">
	<div class='wrapper-banner'>Novidades</div>
</div>

<div class="container-corpo">
	<div class="wrapper-corpo wrapper-corpo-novidades">

	<div class="div-padding-bottom">
		
	<?php foreach ($novidades as $key => $novidade): ?>
		
		<div class="linha-novidade">
			<div class="thumb-novidade"><a href="<?php echo base_url('novidades').'/'.$novidade['slug'] ?> "><img src="<?php echo base_url('assets/img/uploads/novidades/'.$novidade['imagem'].'_thumb.jpg') ?>" alt=""></a> </div>
			<div class="titulo-novidade"><a href="<?php echo base_url('novidades').'/'.$novidade['slug'] ?> "><?php echo $novidade['titulo'] ?></a></div>
			<div class="chamada-novidade"><a href="<?php echo base_url('novidades').'/'.$novidade['slug'] ?> "><?php echo $novidade['excerpt'] ?></a></div>
			<div class="data-novidade"><a href="<?php echo base_url('novidades').'/'.$novidade['slug'] ?> ">
				<?php 
			echo strftime("%d %B ", strtotime( $novidade['data_publicacao'] )); ?>

			</div></a>
			<div class="borda-novidade"></div>
		</div>

	<?php endforeach ?>
	</div>
		
	</div>
	
</div>
