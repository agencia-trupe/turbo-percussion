<div class="main main-home">
	<div class="container-main">
		<div class="slideshow">
			<div class="cycle-box">					
				<?php foreach ($slides_home as $key => $slide): ?>					
					<a href="<?php echo $slide['link'] ?>" title="<?php echo $slide['titulo'] ?>" class="cycles">
						<img class='slide-home' src='<?php echo base_url('assets/img/uploads/slides_home'.'/'.$slide['url_imagem']) ?>' alt='' >					
					</a>
				<?php endforeach ?>
			</div>
		</div>
		<div class="slide-nav">
		

		</div>
		<div class="destaques">
			<div class="titulo-destaques"><span>Destaques</span></div>
			<?php foreach ($destaques as $key => $destaque): ?>
				<a href="<?php echo base_url('produtos/visualizar/'.$destaque['produto']['slug']) ?> "><img class='destaque-thumb' src="<?php echo base_url('assets/img/uploads/produto_fotos/'.$destaque['produto']['imagem_principal'].'_destaque.jpg') ?>" alt=""></a>
				<a href="<?php echo base_url('produtos/visualizar/'.$destaque['produto']['slug']) ?> "><div class="destaque-nome"><?php echo $destaque['produto']['nome'] ?></div></a>
			<?php endforeach ?>
		</div>
	</div>
</div>

<div class="footer-home">
	<div class="container-footer-home">
		<div class="coluna-footer-home">
				<div class="titulo-footer-home">
					<a href="<?php echo base_url('endorses') ?>">Endorses</a>
				</div>			<div class="texto-footer-home">
				<a href="<?php echo base_url('endorses') ?>">Conheça os músicos que recomendam os produtos da Turbo Percussion</a>
			</div>
		</div>
		<div class="coluna-footer-home">
			<div class="titulo-footer-home">
				<a href="<?php echo base_url('downloads') ?>">Downloads</a>
			</div>
			<div class="texto-footer-home">
				<a href="<?php echo base_url('downloads'); ?>">Faça o download de manuais, wallpapers e outros arquivos em PDF</a>
			</div>

		</div>
	
		<div class="coluna-footer-home-2">		<div class="titulo-footer-home">
				Newsletter
			</div>
			
			<form id='form-newsletter' action="<?php echo base_url('home/salva_newsletter') ?> " method='post'>
				<div class="container-inputs-newsletter">
					<div class="input-newsletter">
						<input type="text" name='newsletter[nome]'placeholder='nome'>
					</div>
					<div class="input-newsletter">
						<input type="text" name='newsletter[email]' placeholder='e-mail'>
					</div>
				</div>
				<button class="enviar-newsletter">

					<div type='submit' class="">enviar</div>
				</button>
			</form>
			</div>
	</div>	
</div>



<script>
	jQuery(document).ready(function($) {
		$('.cycle-box').cycle({
			fx: 'scrollRight'  ,
			speed: 1400,
			timeout: 8000,
			pager:  '.slide-nav',
			activePagerClass: 'activeSlide',
			manualTrump: false
		});
		$('.slide-nav a').html('');
		
		$('.enviar-newsletter').on('click',function(){
			$('#form-newsletter').submit();
		})
		<?php if ($this->session->flashdata('newsletter_sucesso')): ?>
		    alert('<?php echo $this->session->flashdata('newsletter_sucesso') ?> ');
    
  		<?php endif ?>

	});


	

</script>
