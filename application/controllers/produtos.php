<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start(); //we need to call PHP's session object to access it through CI
class Produtos extends CI_Controller {
  
  public $data;

 function __construct()
 {
  
   	parent::__construct();

   	$this->data['page'] = 'produtos';
	$this->data['contato'] = $this->db->get('contato_principal')->row_array();
    $this->data['parceiros'] = $this->db->get('parceiros')->result_array();   
 }

 function index($paginacao = 0)
 {
	$this->data['produto_categorias'] = $this->db->order_by('ordem')->get('produto_categorias')->result_array();
	foreach($this->data['produto_categorias'] as $key => $value){
		$this->data['produto_categorias'][$key]['produto_subcategorias'] = $this->db->order_by('ordem')->get_where('produto_subcategorias', array('id_categoria' => $value['id']))->result_array();		
	}

	$this->data['slug']  = false;
	$this->load->library('pagination');
	$this->data['total'] = $this->db->get('produtos')->result_array();
	$this->data['produtos_destaque'] = $this->db->order_by('id', 'desc')->get('produtos',9, ($paginacao) ? --$paginacao * 9 : 0  )->result_array();

	$config['base_url'] = base_url('produtos');
	$config['total_rows'] = count($this->data['total']);
	
	#$config['total_rows'] = 200;

	$config['uri_segment'] = 2;

	$config['use_page_numbers'] = TRUE;
	$config['anchor_class'] = " class='item-paginacao' ";
	$config['cur_tag_open'] = " <div class='item-paginacao item-ativo'> ";
	$config['prev_link'] = " anterior";
	$config['next_link'] = " próxima ";
	$config['first_link'] = " primeira ";
	$config['last_link'] = " última ";
	$config['cur_tag_close'] = " </div> ";
	$config['per_page'] = 9; 

	$this->pagination->initialize($config); 

    $this->data['pagination'] = $this->pagination->create_links();




     $this->data['title'] = 'Produtos';
     $this->load->view('layout/template', $this->data);

}


 function pesquisar($query, $paginacao = 0)
 {
	$this->data['produto_categorias'] = $this->db->order_by('ordem')->get('produto_categorias')->result_array();
	foreach($this->data['produto_categorias'] as $key => $value){
		$this->data['produto_categorias'][$key]['produto_subcategorias'] = $this->db->order_by('ordem')->get_where('produto_subcategorias', array('id_categoria' => $value['id']))->result_array();		
	}

	$this->data['slug']  = false;
	$this->load->library('pagination');
	$config['per_page'] = 9;

$termo = str_replace(' ', '%', urldecode($query));
$query_busca = <<<QRY
SELECT * FROM produtos WHERE
nome LIKE '%{$termo}%' OR
descricao LIKE '%{$termo}%' OR
informacoes LIKE '%{$termo}%'
QRY;
	
	$this->data['total'] = $this->db->query($query_busca)->num_rows();

	$query_busca .= " LIMIT {$paginacao}, {$config['per_page']}";
	$this->data['produtos_destaque'] = $this->db->query($query_busca)->result_array();

	$config['base_url'] = base_url('produtos/pesquisar/'.$query);
	$config['total_rows'] = $this->data['total'];	
	$config['uri_segment'] = 4;

	$config['use_page_numbers'] = FALSE;
	$config['anchor_class'] = " class='item-paginacao' ";
	$config['cur_tag_open'] = " <div class='item-paginacao item-ativo'> ";
	$config['prev_link'] = " anterior";
	$config['next_link'] = " próxima ";
	$config['first_link'] = " primeira ";
	$config['last_link'] = " última ";
	$config['cur_tag_close'] = " </div> ";
	$config['num_links'] = 10;	

	$this->pagination->initialize($config); 

    $this->data['pagination'] = $this->pagination->create_links();

     $this->data['title'] = 'Produtos';
     $this->load->view('layout/template', $this->data);
}

function visualizar($slug = false){

	$this->data['title'] = 'Produtos';
	$this->data['produto_categorias'] = $this->db->get('produto_categorias')->result_array();
	foreach($this->data['produto_categorias'] as $key => $value){
		$this->data['produto_categorias'][$key]['produto_subcategorias'] = $this->db->order_by('ordem')->get_where('produto_subcategorias', array('id_categoria' => $value['id']))->result_array();
	}	

    $this->data['produto'] = $this->db->get_where('produtos', array('slug' => $slug))->row_array();

    $this->data['produto']['fotos_produto'] = $this->db->get_where('produto_fotos', array('id_produto' => $this->data['produto']['id']))->result_array();
    $this->data['produto']['caracteristicas_produto'] = $this->db->get_where('produto_caracteristicas', array('id_produto' => $this->data['produto']['id']))->result_array();
    $this->data['page'] = 'visualizar';

	$this->data['categoria_selecionada'] = $this->db->get('produto_categorias', array('id' => $this->data['produto']['id']));
	 
	$this->data['seo'] = array(
		'imagem' => 'assets/img/uploads/produto_fotos/'.$this->data['produto']['imagem_principal'],
		'titulo' => htmlentities($this->data['produto']['nome'])
	);
    $this->data['title'] = 'Visualizar Produto';
     
    $this->load->view('layout/template', $this->data);
}

function download($id){
    $this->load->helper('download');
   	$file = $this->db->get_where('produtos', array('id' => $id))->row();
   	$ch = curl_init(); 
   	curl_setopt($ch, CURLOPT_URL, base_url('assets/img/uploads/produto_manuais/'.$file->url_manual)); 
   	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
   	$data = curl_exec($ch);
   	curl_close($ch);
      
    force_download($file->url_manual, $data );
}


 function subcategoria($slug = false, $paginacao = 0)
 {
 	 
 	 if(!$slug){
 	 	redirect(base_url('produtos'), 'refresh');
 	 }
	$this->data['produto_categorias'] = $this->db->order_by('ordem')->get('produto_categorias')->result_array();
	foreach($this->data['produto_categorias'] as $key => $value){
		$this->data['produto_categorias'][$key]['produto_subcategorias'] = $this->db->order_by('ordem')->get_where('produto_subcategorias', array('id_categoria' => $value['id']))->result_array();
	}
	$this->load->library('pagination');

    $subcategoria = $this->db->get_where('produto_subcategorias', array('slug' => $slug))->row();
	$this->data['total'] = $this->db->get_where('produtos', array('id_subcategoria' => $subcategoria->id))->result_array();
	$this->data['produtos_destaque'] = $this->db->order_by('ordem')->get_where('produtos', array('id_subcategoria' => $subcategoria->id), 9, ($paginacao) ? --$paginacao * 9 : 0)->result_array();
	$this->data['slug'] = $slug;
	$config['base_url'] = base_url('produtos/subcategoria/'.$slug);
	$config['total_rows'] = count($this->data['total']);
	$config['uri_segment'] = 4;
	$config['use_page_numbers'] = TRUE;
	$config['anchor_class'] = " class='item-paginacao' ";
	$config['cur_tag_open'] = " <div class='item-paginacao item-ativo'> ";
	$config['prev_link'] = " anterior";
	$config['next_link'] = " próxima ";
	$config['first_link'] = " primeira ";
	$config['last_link'] = " última ";
	$config['cur_tag_close'] = " </div> ";
	$config['per_page'] = 9; 


	$this->pagination->initialize($config); 

    $this->data['pagination'] = $this->pagination->create_links();



     $this->data['title'] = 'Produtos';
     $this->load->view('layout/template', $this->data);

}

   
 }



?>

