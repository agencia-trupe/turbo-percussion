<div class="row-fluid">
	<div class="col-lg-12"><?php if ($this->session->flashdata('message')): ?>
		<div class="alert alert-success"> <?php echo $this->session->flashdata('message') ?> </div>
		
	<?php endif ?>
	</div>
</div>

<div class="row-fluid">
	<legend>Editar <?php echo $title ?></legend>
</div>
<div class="clearfix"></div>

<form action="<?php echo base_url('admin/'.$attr['slug'].'/'.$this->uri->segment(3)).'/'.$obj->id ?>" method='post'>
	<div class="row-fluid">
		<div class="col-1 text-right">
			<label for="titulo" class='control-label'>Título</label>
		</div>
		<div class="col-3">
			<input class='form-control' type="text" name='<?php echo $attr['slug'] ?>[titulo]' value='<?php echo ($obj) ? $obj->titulo : '' ?>' id='titulo'>	
		</div>
	</div><div class="clearfix"></div>
	<input type="hidden" name='<?php echo $attr['slug'] ?>[id_servico] ' value='<?php echo $this->session->userdata('categoria_selecionada') ?>'>
		<div class="row-fluid">
		<div class="col-1 text-right">
			<label for="texto" class='control-label'>Texto</label>
		</div>
		<div class="col-10">
			<textarea class='form-control mce' type="text" rows='10' name='<?php echo $attr['slug'] ?>[texto]' value="" id='texto'><?php echo ($obj) ? $obj->texto : '' ?></textarea>	
		</div>
	</div><div class="clearfix"></div>





	<div class="row-fluid">
		<div class="col-1 ">
			
		</div>
		<div class="col-2">
			<button type='submit' class='btn btn-sm btn-success'>Salvar</button>
			<button type='button' onClick='history.back()' class='btn btn-sm btn-info'>Voltar</button>
		</div>
	</div><div class="clearfix"></div>



</form>






