<div class="row-fluid">
	<div class="col-lg-12"><?php if ($this->session->flashdata('message')): ?>
		<div class="alert alert-success"> <?php echo $this->session->flashdata('message') ?> </div>
		
	<?php endif ?>
	</div>
</div>

<div class="row-fluid">
	<legend><?php echo $title ?></legend>
</div>
<div class="clearfix"></div>

<form action="<?php echo base_url('admin/'.$attr['slug'].'/'.$this->uri->segment(3).'/'.$obj->id) ?>" method='post'>
	<div class="row-fluid">
		<div class="col-1 text-right">
			<label for="origem" class='control-label'>Origem</label>
		</div>
		<div class="col-3">
			<input class='form-control' type="text" name='<?php echo $attr['slug'] ?>[origem]' value='<?php echo ($obj) ? $obj->origem : '' ?>' id='origem'>	
		</div>
	</div><div class="clearfix"></div>

	<div class="row-fluid">
		<div class="col-1 text-right">
			<label for="destino" class='control-label'>Destino</label>
		</div>
		<div class="col-3">
			<input class='form-control' type="text" name='<?php echo $attr['slug'] ?>[destino]' value='<?php echo ($obj) ? $obj->destino : '' ?>' id='destino'>	
		</div>
	</div><div class="clearfix"></div>

<div class="row-fluid">
		<div class="col-1 text-right">
			<label for="saida" class='control-label'>Saída</label>
		</div>
		<div class="col-3">
			<input class='form-control' type="text" name='<?php echo $attr['slug'] ?>[saida]' value='<?php echo ($obj) ? $obj->saida : '' ?>' id='saida'>	
		</div>
	</div><div class="clearfix"></div>

<div class="row-fluid">
		<div class="col-1 text-right">
			<label for="chegada" class='control-label'>Chegada</label>
		</div>
		<div class="col-3">
			<input class='form-control' type="text" name='<?php echo $attr['slug'] ?>[chegada]' value='<?php echo ($obj) ? $obj->chegada : '' ?>' id='chegada'>	
		</div>
	</div><div class="clearfix"></div>


<div class="row-fluid">
		<div class="col-1 text-right">
			<label for="titulo" class='control-label'>Horário</label>
		</div>
		<div class="col-3">
			<select name="<?php echo $attr['slug'] ?>[id_horario_rota]" class='form-control' id="">
				<option selected disabled value="">Escolha um horário...</option>
				<?php foreach ($horarios_rota as $key => $value): ?>
					<option value="<?php echo $value->id ?> "<?php if ($value->id == $obj->id_horario_rota): ?>selected ='selected' 	<?php endif ?> ><?php echo $value->titulo ?> </option>
					
				<?php endforeach ?>
			</select>
		</div>
	</div><div class="clearfix"></div>



	<div class="row-fluid">
		<div class="col-1 ">
			
		</div>
		<div class="col-2">
			<button type='submit' class='btn btn-sm btn-success'>Salvar</button>
			<button type='button' onClick='history.back()' class='btn btn-sm btn-info'>Voltar</button>
		</div>
	</div><div class="clearfix"></div>



</form>



